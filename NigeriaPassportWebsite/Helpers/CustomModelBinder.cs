﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Globalization;
using System.Reflection;
using PassportCommon.Utilities.CustomModelAttributes;
using System.Web.Script.Serialization;
using System.IO;
using PassportProxy;

namespace Passport.Helpers
{
    public class CustomModelBinder : DefaultModelBinder
    {
        protected override void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
        {
            if (propertyDescriptor.PropertyType == typeof(string))
            {
                // always trim strings to clean up database padding
                if (value != null &&
                    (string)value != string.Empty)
                {
                    value = ((string)value).Trim();

                    if (bindingContext.ValueProvider.GetValue(propertyDescriptor.Name) != null
                        && bindingContext.ValueProvider.GetValue(propertyDescriptor.Name).AttemptedValue != null)
                    {
                        if (propertyDescriptor.Attributes[typeof(TitleCaseAttribute)] != null)
                        {
                            value = PassportCommon.Utilities.GeneralUtilities.TitleCaseString((string)value);
                        }

                        if (propertyDescriptor.Attributes[typeof(NameValidationAttribute)] != null)
                        {
                            this.ValidateNameProperty(controllerContext, bindingContext, propertyDescriptor, ref value);
                        }

                        if (propertyDescriptor.Attributes[typeof(PhoneNumberNigeriaAttribute)] != null)
                        {
                            this.ValidatePhoneNumberNigeriaProperty(controllerContext, bindingContext, propertyDescriptor, ref value);
                        }
                    }
                }
            }
            else if (propertyDescriptor.PropertyType == typeof(System.DateTime))
            {
                if (bindingContext.ValueProvider.GetValue(propertyDescriptor.Name) != null
                        && bindingContext.ValueProvider.GetValue(propertyDescriptor.Name).AttemptedValue != null)
                {
                    if (propertyDescriptor.Attributes[typeof(FormatDateTimeAttribute)] != null)
                    {
                        DateTime datevalue;
                        this.DeserializeDateProperty(controllerContext, bindingContext, propertyDescriptor, ref value, out datevalue);
                    }

                    if (propertyDescriptor.Attributes[typeof(FormatDateOfBirthAttribute)] != null)
                    {
                        DateTime datevalue;
                        this.DeserializeDateProperty(controllerContext, bindingContext, propertyDescriptor, ref value, out datevalue);

                        this.ValidateDateOfBirthProperty(controllerContext, bindingContext, propertyDescriptor, datevalue);
                    }
                }
            }

            base.SetProperty(controllerContext, bindingContext, propertyDescriptor, value);
        }

        //Send out date value as well so that date convert issues in i.e. date of birth checking does not happen again
        private void DeserializeDateProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, ref object value, out DateTime datevalue)
        {
            //Default modelbinder does not deserialize datetime value very well as a rule, decorate the model property with the FormatDateTimeAttribute and give it a specific format if necessary
            datevalue = DateTime.MinValue;
            var model = bindingContext.Model;
            PropertyInfo property = model.GetType().GetProperty(propertyDescriptor.Name);

            var val = bindingContext.ValueProvider.GetValue(propertyDescriptor.Name);

            //Check all attributes that use this deserialize method for a custom format
            string format = string.Empty;
            if (propertyDescriptor.Attributes[typeof(FormatDateTimeAttribute)] != null)
            {
                format = ((FormatDateTimeAttribute)propertyDescriptor.Attributes[typeof(FormatDateTimeAttribute)]).DateFormat;
            }
            else if (propertyDescriptor.Attributes[typeof(FormatDateOfBirthAttribute)] != null)
            {
                format = ((FormatDateOfBirthAttribute)propertyDescriptor.Attributes[typeof(FormatDateOfBirthAttribute)]).DateFormat;
            }
            
            if (string.IsNullOrWhiteSpace(format))
            {
                format = "dd/MM/yyyy";
            }

            if (val != null)
            {
                DateTime exactdatevalue;

                if (DateTime.TryParseExact(val.AttemptedValue, format, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out exactdatevalue))
                {
                    value = exactdatevalue;
                    datevalue = exactdatevalue;
                }
            }
        }

        private void ValidateDateOfBirthProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, DateTime value)
        {
            int maxage = ((FormatDateOfBirthAttribute)propertyDescriptor.Attributes[typeof(FormatDateOfBirthAttribute)]).MaxAge;
            int minage = ((FormatDateOfBirthAttribute)propertyDescriptor.Attributes[typeof(FormatDateOfBirthAttribute)]).MinAge;

            if (value > DateTime.Now.AddYears(-minage))
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Date of birth is invalid. Minimum age required is " + minage + ".");
            }

            if (value < DateTime.Now.AddYears(-maxage))
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Date of birth is invalid. User cannot be older than " + maxage + ".");
            }

            if (value > DateTime.Today)
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Date of birth is invalid. Date cannot be in the future.");
            }
        }

        private void ValidatePhoneNumberNigeriaProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, ref object value)
        {
            MessageProxy proxy = new MessageProxy();

            if (!PassportCommon.Utilities.PhoneNumberUtil.IsValidNigerianNumber(value.ToString(), proxy.channel.GetValidGsmCacheList()))
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "The mobile number you entered is not valid. Please enter a valid mobile number.");
            }
        }

        private void ValidateNameProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, ref object value)
        {
            int maxlength = ((NameValidationAttribute)propertyDescriptor.Attributes[typeof(NameValidationAttribute)]).MaxLength;
            int minlength = ((NameValidationAttribute)propertyDescriptor.Attributes[typeof(NameValidationAttribute)]).MinLength;
            string name = value.ToString();

            if (name.Any(char.IsDigit))
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Numbers are not allowed.");
            }

            if (name.Length > maxlength)
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Field cannot be longer than " + maxlength + " characters.");
            }

            if (name.Length < minlength)
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Field cannot be shorter than " + minlength + " characters.");
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Whitespace characters are not allowed.");
            }

            value = PassportCommon.Utilities.GeneralUtilities.TitleCaseString((string)value);
        }
    }
}