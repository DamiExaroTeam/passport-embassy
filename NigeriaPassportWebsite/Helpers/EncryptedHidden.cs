﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;

namespace Passport.Helpers
{
    public static class EncryptedHidden
    {
        public static string CustHiddenFor(string name,string value)
        {
            return String.Format(@"<input data-val=""true"" id=""{0}"" name=""{0}"" type=""hidden"" value=""{1}"" />",name,value);
        }

        public static MvcHtmlString HiddenForEncrypted<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> ex)
        {
            var metadata = ModelMetadata.FromLambdaExpression(ex, htmlHelper.ViewData);
            string name = ExpressionHelper.GetExpressionText(ex);
            string value = metadata.Model.ToString();
            string encryptedValue = EncryptString(value);
            return MvcHtmlString.Create(CustHiddenFor(name, encryptedValue));
        }

        private static string EncryptString(string value)
        {
            return PassportCommon.Security.EncryptPassword.EncryptString(value);            
        }
                

    }
}