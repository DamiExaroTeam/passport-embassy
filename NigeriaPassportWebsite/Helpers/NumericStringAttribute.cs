﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Passport.Helpers
{
    public class NumericStringAttribute : ValidationAttribute
    {
        private const string _defaultErrorMessage = "{0} may only contain numeric characters.";

        public NumericStringAttribute(string fieldName)
            : base(_defaultErrorMessage) 
        {
            FieldName = fieldName;
        }

        public string FieldName { get; set; }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString, FieldName);
        } 

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            string sValue = value.ToString();

            return PassportCommon.Utilities.GeneralUtilities.IsStringOnlyNumeric(sValue);
        }
    }
}