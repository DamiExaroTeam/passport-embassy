﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportProxy;
using Passport.Models.Session;

namespace Passport.Helpers
{
    public class RestrictAccess : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (SessionData.WebAccountId > 0)
            {
                bool loggedout = false;
                AuditTrailProxy proxy = new AuditTrailProxy();
                //if (!proxy.channel.LogWebAccountActivity(SessionData.WebAccountId, SessionData.UserIP, ref loggedout) ||
                //    loggedout)
                //{
                //    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary {{"controller", "Security"}, 
                //                                                                                              {"action", "ShowLoggedOutStatus"}});
                //    return;
                //}
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary {{"controller", "Security"}, 
                                                                                                              {"action", "ShowLoggedOutStatus"}});
                return;
            }


            string sURL = "/" + filterContext.RouteData.Values["Controller"] + "/" + filterContext.RouteData.Values["Action"];

            string controller = filterContext.RouteData.Values["Controller"].ToString();
            string action = filterContext.RouteData.Values["Action"].ToString();

            List<PassportCommon.Models.Security.InternalActionModel> internalAction = Passport.Models.Session.SessionData.AllowedUserActions;

            int count = (from c in internalAction
                         where c.CONTROLLER.ToLower() == controller.ToLower()
                         && c.ACTION.ToLower() == action.ToLower()
                         select c).Count();


            if (count == 0)
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary {{"controller", "Home"}, 
                                                                                                              {"action", "Index"}});
            }
        }
    }
}