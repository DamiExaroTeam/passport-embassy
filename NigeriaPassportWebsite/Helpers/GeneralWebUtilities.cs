﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace Passport.Helpers
{
    public class GeneralWebUtilities
    {
        public static void PopulateDropDownList(System.Web.UI.WebControls.DropDownList dropdownlist, List<PassportCommon.Models.General.DropDownListModel> list)
        {
            dropdownlist.DataSource = list;
            dropdownlist.DataValueField = "Value";
            dropdownlist.DataTextField = "Text";
            dropdownlist.DataBind();
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list)
        {
            return ReturnSelectListFromList(list, null, false);
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list, object selectedvalue)
        {
            return ReturnSelectListFromList(list, selectedvalue, false);
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list, bool AddEmptyValue)
        {
            return ReturnSelectListFromList(list, null, false);
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list, object selectedvalue, bool AddEmptyValue)
        {
            if (AddEmptyValue || list.Count == 0)
            {
                list.Insert(0, new PassportCommon.Models.General.DropDownListModel(-1, ""));
            }

            if (selectedvalue == null)
            {
                selectedvalue = list[0].Value;
            }

            foreach  (PassportCommon.Models.General.DropDownListModel model in list)
            {
                model.Text = model.Text.Replace("_", " ");
            }

            SelectList selectList = new SelectList(list, "Value", "Text", selectedvalue.ToString());
            return selectList;
        }

        public static SelectList GetEmptySelectList()
        {
            List<PassportCommon.Models.General.DropDownListModel> list = new List<PassportCommon.Models.General.DropDownListModel>();

            return new SelectList(list);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter)
        {
            return FilterSelectList(list, listfilter, null, false);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter, bool addemptyvalue)
        {
            return FilterSelectList(list, listfilter, null, addemptyvalue);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter, object selectedvalue)
        {
            return FilterSelectList(list, listfilter, selectedvalue, false);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter, object selectedvalue, bool addemptyvalue)
        {
            List<PassportCommon.Models.General.DropDownListWithFilter> filteredlist = (from li in list
                                                                             where li.Filter.ToString() == listfilter.ToString()
                                                                             select li).ToList<PassportCommon.Models.General.DropDownListWithFilter>();

            if (filteredlist.Count == 0)
            {
                return GetEmptySelectList();
            }

            if (addemptyvalue)
            {
                filteredlist.Insert(0, new PassportCommon.Models.General.DropDownListWithFilter(null, "", null));
            }

            if (selectedvalue == null && filteredlist.Count > 0)
            {
                selectedvalue = filteredlist[0].Value;
            }

            SelectList selectList = new SelectList(filteredlist, "Value", "Text", selectedvalue);
            return selectList;
        }

        public static SelectList SetSelectListSelectedValue(SelectList list, object selectedvalue)
        {
            var selected = list.Where(x => x.Value == selectedvalue.ToString()).First();
            selected.Selected = true;

            return list;
        }

        public static PassportCommon.Models.General.UserInfoModel GetUserInfo()
        {
            PassportCommon.Models.General.UserInfoModel user = new PassportCommon.Models.General.UserInfoModel();
            user.UserDetails = "REMOTE_HOST: " + HttpContext.Current.Request.ServerVariables["REMOTE_HOST"] + "REMOTE_USER: " + HttpContext.Current.Request.ServerVariables["REMOTE_USER"] + "AUTH_USER: " + HttpContext.Current.Request.ServerVariables["AUTH_USER"];
            user.UserIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null ? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString() : HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            return user;
        }
    }
}