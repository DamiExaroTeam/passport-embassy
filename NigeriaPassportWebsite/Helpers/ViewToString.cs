﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using System.Web.Mvc.Html;
using System.ComponentModel;

namespace Passport.Helpers
{
    public static class ViewToString
    {
        /// <summary>Renders a view to string.</summary> 
        public static string RenderPartialToString(this HtmlHelper html, string viewName, object viewData)
        {
            return RenderViewToString(html.ViewContext.Controller.ControllerContext, viewName, viewData);
        }
        /// <summary>Renders a view to string.</summary> 
        public static string RenderViewToString(this Controller controller, string viewName, object viewData)
        {
            return RenderViewToString(controller.ControllerContext, viewName, viewData);
        }

        private static string RenderViewToString(ControllerContext context, string viewName, object viewData)
        {
            //Create memory writer 
            var sb = new StringBuilder();
            var memWriter = new StringWriter(sb);

            //Create fake http context to render the view 
            var fakeResponse = new HttpResponse(memWriter);
            var fakeContext = new HttpContext(HttpContext.Current.Request, fakeResponse);
            var fakeControllerContext = new ControllerContext(
                new HttpContextWrapper(fakeContext),
                context.RouteData, context.Controller);

            var oldContext = HttpContext.Current;
            HttpContext.Current = fakeContext;

            var dataDictionary = new ViewDataDictionary();
            var tempdataDictionary = new TempDataDictionary();

            //foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(viewData))
            //{
            //    object val = prop.GetValue(viewData);
            //    dataDictionary.Add(prop.Name,val);
            //    tempdataDictionary.Add(prop.Name, val);                
            //}

            var viewContext = new ViewContext(fakeControllerContext, new FakeView(), dataDictionary, tempdataDictionary, memWriter);

            var container = new ViewPage();
            container.ViewContext = viewContext;
            container.ViewData = dataDictionary;

            //Use HtmlHelper to render partial view to fake context 
            var html = new HtmlHelper(viewContext, container);
            
            html.RenderPartial(viewName,viewData, dataDictionary);

            //Restore context 
            HttpContext.Current = oldContext;

            //Flush memory and return output 
            memWriter.Flush();
            return sb.ToString();
        }

        /// <summary>Fake IView implementation, only used to instantiate an HtmlHelper.</summary> 
        public class FakeView : IView
        {
            #region IView Members
            public void Render(ViewContext viewContext, System.IO.TextWriter writer)
            {
                throw new NotImplementedException();
            }
            #endregion
        }

    }
}
