﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Passport.Helpers
{
    public static class StringHelpers
    {
        public static string CutStringLength(this HtmlHelper helper, string text, int characters)
        {
            if (characters <= 3)
                throw new ApplicationException("The amount of characters for CutStringLength cannot be less than 3");

            if (text.Length > characters)
                return text.Substring(0, (characters - 3)) + "...";
            else
                return text;
        }
    }
}