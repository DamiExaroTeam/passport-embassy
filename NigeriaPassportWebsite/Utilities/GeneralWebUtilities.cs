﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;
using System.Web.UI;

namespace PassportIntraWebsite.Utilities
{
    public static class GeneralWebUtilities
    {
        public static void PopulateDropDownList(System.Web.UI.WebControls.DropDownList dropdownlist, List<PassportCommon.Models.General.DropDownListModel> list)
        {
            dropdownlist.DataSource = list;
            dropdownlist.DataValueField = "Value";
            dropdownlist.DataTextField = "Text";
            dropdownlist.DataBind();
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list)
        {
            return ReturnSelectListFromList(list, null, false);
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list, object selectedvalue)
        {
            return ReturnSelectListFromList(list, selectedvalue, false);
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list, bool AddEmptyValue)
        {
            return ReturnSelectListFromList(list, null, AddEmptyValue);
        }

        public static SelectList ReturnSelectListFromList(List<PassportCommon.Models.General.DropDownListModel> list, object selectedvalue, bool AddEmptyValue)
        {
            if (AddEmptyValue || list.Count == 0)
            {
                list.Insert(0, new PassportCommon.Models.General.DropDownListModel(-1, ""));
            }

            if (selectedvalue == null && list.Count > 0)
            {
                selectedvalue = list[0].Value;
            }

            foreach  (PassportCommon.Models.General.DropDownListModel model in list)
            {
                model.Text = model.Text.Replace("_", " ");
            }

            SelectList selectList = new SelectList(list, "Value", "Text", selectedvalue.ToString());
            return selectList;
        }

        public static SelectList ReturnSelectListFromList<T>(List<T> list, string valuefield, string textfield, object selectedvalue, bool AddEmptyValue) where T : class
        {
            var properties = typeof(T).GetProperties();
            var valueproperty = properties.Where(x => x.Name == valuefield).Single();
            var textproperty = properties.Where(x => x.Name == textfield).Single();

            if (AddEmptyValue || list.Count == 0)
            {
                T emptymodel = (T)System.Activator.CreateInstance<T>();

                if (valueproperty.PropertyType == typeof(System.String))
                {
                    valueproperty.SetValue(emptymodel, "", null);
                }
                else
                {
                    valueproperty.SetValue(emptymodel, -1, null);
                }

                textproperty.SetValue(emptymodel, "", null);

                list.Insert(0, emptymodel);
            }

            if (selectedvalue == null)
            {
                selectedvalue = valueproperty.GetValue(list[0], null);
            }

            SelectList selectList = new SelectList(list, valuefield, textfield, selectedvalue.ToString());
            return selectList;
        }

        public static SelectList GetEmptySelectList()
        {
            List<PassportCommon.Models.General.DropDownListModel> list = new List<PassportCommon.Models.General.DropDownListModel>();

            return new SelectList(list);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter)
        {
            return FilterSelectList(list, listfilter, null, false);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter, bool addemptyvalue)
        {
            return FilterSelectList(list, listfilter, null, addemptyvalue);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter, object selectedvalue)
        {
            return FilterSelectList(list, listfilter, selectedvalue, false);
        }

        public static SelectList FilterSelectList(List<PassportCommon.Models.General.DropDownListWithFilter> list, object listfilter, object selectedvalue, bool addemptyvalue)
        {
            List<PassportCommon.Models.General.DropDownListWithFilter> filteredlist = (from li in list
                                                                             where li.Filter.ToString() == listfilter.ToString()
                                                                             select li).ToList<PassportCommon.Models.General.DropDownListWithFilter>();

            if (filteredlist.Count == 0)
            {
                return GetEmptySelectList();
            }

            if (addemptyvalue)
            {
                filteredlist.Insert(0, new PassportCommon.Models.General.DropDownListWithFilter(null, "", null));
            }

            if (selectedvalue == null && filteredlist.Count > 0)
            {
                selectedvalue = filteredlist[0].Value;
            }

            SelectList selectList = new SelectList(filteredlist, "Value", "Text", selectedvalue);
            return selectList;
        }

        public static SelectList FilterSelectList<T>(List<T> list, string valuefield, string textfield, string filterfield, object listfilter, object selectedvalue, bool addemptyvalue) where T : class
        {
            var properties = typeof(T).GetProperties();
            var valueproperty = properties.Where(x => x.Name == valuefield).Single();
            var textproperty = properties.Where(x => x.Name == textfield).Single();
            var filterproperty = properties.Where(x => x.Name == filterfield).Single();

            List<T> filteredlist = (from li in list
                                    where filterproperty.GetValue(li, null).ToString() == listfilter.ToString()
                                    select li).ToList<T>();

            if (filteredlist.Count == 0)
            {
                return GetEmptySelectList();
            }

            if (addemptyvalue)
            {
                T emptymodel = (T)System.Activator.CreateInstance<T>();

                if (valueproperty.PropertyType == typeof(System.String))
                {
                    valueproperty.SetValue(emptymodel, "", null);
                }
                else
                {
                    valueproperty.SetValue(emptymodel, -1, null);
                }

                textproperty.SetValue(emptymodel, "", null);

                filteredlist.Insert(0, emptymodel);
            }

            if (selectedvalue == null && filteredlist.Count > 0)
            {
                selectedvalue = valueproperty.GetValue(filteredlist[0], null);
            }

            SelectList selectList = new SelectList(filteredlist, valuefield, textfield, selectedvalue);
            return selectList;
        }

        public static SelectList SetSelectListSelectedValue(SelectList list, object selectedvalue)
        {
            var selected = list.Where(x => x.Value == selectedvalue.ToString()).First();
            selected.Selected = true;

            return list;
        }

        public static PassportCommon.Models.General.UserInfoModel GetUserInfo()
        {
            PassportCommon.Models.General.UserInfoModel user = new PassportCommon.Models.General.UserInfoModel();
            user.UserDetails = "REMOTE_HOST: " + HttpContext.Current.Request.ServerVariables["REMOTE_HOST"] + "REMOTE_USER: " + HttpContext.Current.Request.ServerVariables["REMOTE_USER"] + "AUTH_USER: " + HttpContext.Current.Request.ServerVariables["AUTH_USER"];
            user.UserIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null ? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString() : HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            user.UserWebAccountId = Passport.Models.Session.SessionData.WebAccountId;

            return user;
        }

        public static string RenderPartialViewToHTMLString(this Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;

            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string RenderPartialViewToHTMLString(string controlName, object viewData)
        {
            ViewPage viewPage = new ViewPage() { ViewContext = new ViewContext() };

            viewPage.ViewData = new ViewDataDictionary(viewData);
            viewPage.Controls.Add(viewPage.LoadControl(controlName));

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter tw = new HtmlTextWriter(sw))
                {
                    viewPage.RenderControl(tw);
                }
            }

            return sb.ToString();
        }
    }
}