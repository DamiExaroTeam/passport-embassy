﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<PassportCommon.Models.Menu.Menu>>" %>

<%--<script src="<%: Url.Content("~/Scripts/ui//jquery.ui.accordion.js") %>" type="text/javascript"></script>--%>
<%--
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({
                event: "mouseover", 
                autoHeight: false,
                icons: false
            });
        });
	</script>--%>
    
    <div id="AccordianMenu" class="roundedTop">   
        <% foreach (var item in Model) { %>
            <div class="optionbox roundedTop shadow">
              <div class="optionboxheader roundedTop"><%: item.NAME%></div>
              <div class="optionboxoption">
                <% foreach (var childItem in item.ChildItems)
                   { %>
                       <div class="MenuItem">
                           <a href="<%: childItem.URL %>"><%: childItem.NAME%></a>
                       </div>
                <% } %>               
              </div>
            </div>
        <% } %>
    </div>
   
    <%--<% if  (PassportCommon.Utilities.GeneralUtilities.IsDeveloper) %>
    <% { %>
         <div id="accordion">   
            <% foreach (var item in Model)
               { %>
                  <a href="#"><%:item.NAME%></a>
                  <div>
                    <% foreach (var childItem in item.ChildItems)
                       { %>
                            <a href="<%:childItem.URL %>"><%:childItem.NAME%></a>
                    <% } %>
                  </div>
            <% } %>
        </div>
    <% } %>--%>
