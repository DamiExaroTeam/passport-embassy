﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    NIGERIA IMMIGRATION SERVICE
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  

    <table id="PageMainContent">
        <tr>
            <th>
                Welcome to the Nigeria Passport Verification Service
            </th>
        </tr>   
        <tr>
            <td>
                New to the site? Please <%: Html.ActionLink("register", "RegisterOperator", "OperatorRegistration")%>
            </td>
        </tr> 
        <tr>
            <td>
                If you have a username and password, go to <%: Html.ActionLink("Login", "Login", "Security")%>
            </td>
        </tr>
    </table>     
</asp:Content>
