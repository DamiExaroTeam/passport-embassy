﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="optionbox roundedTop shadow">
    <div class="optionboxheader roundedTop">    
        <%: Html.Label(ViewData["Message"].ToString()) %><br />
        <% if (ViewData["MessageExtended"] != null && ViewData["MessageExtended"] != string.Empty) %>
        <% { %>
            <small> <%: Html.Label(ViewData["MessageExtended"].ToString())%> </small> <br />        
        <% } %>
    </div>
    <div class="optionboxoption">
        <div class="MenuItem">
            <%: Html.ActionLink("Home", "Index", "Home") %> 
        </div>
        <div class="MenuItem">
            <%: Html.ActionLink(ViewData["ActionTypeText"].ToString(), ViewData["ActionName"].ToString(), ViewData["ControllerName"].ToString()) %>     
        </div>
     </div>
</div>