﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=6.2.12.1017, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<html>
<head id="Header" runat="server">
    <link href="~/Content/Reports.css" rel="stylesheet" type="text/css" />
  
        <script type="text/javascript">        
        function isIE() {
            var myNav = navigator.userAgent.toLowerCase();
            return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
        }

        window.onload = function () 
        {
            if (isIE() < 10) {
                alert('Older versions of Internet Explorer has a visual problem viewing the report. Please upgrade your Internet Explorer. It is still possible to export the report to pdf with older versions.');
            }
        };
    </script>   
</head>
<body>
    <form clientIdmode="Static" id="TelerikForm" runat="server">
        <telerik:ReportViewer ID="ReportViewer1" runat="server" 
            Height="100%" Width="100%">
        </telerik:ReportViewer>
    </form>    
</body>
</html>
<script runat="server">
    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }

    protected override void OnPreRender(EventArgs e)
    {   
        var objectDataSource = new Telerik.Reporting.ObjectDataSource();
        objectDataSource.DataSource = ViewData["Data"];

        Telerik.Reporting.Report rep = (Telerik.Reporting.Report)ViewData["Report"];
        
        rep.DataSource = objectDataSource;                
        ReportViewer1.Report = rep;
        
        base.OnPreRender(e);
    }
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
</script>