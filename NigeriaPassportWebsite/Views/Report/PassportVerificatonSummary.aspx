﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Passport Verificaton Summary Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
    <script type="text/javascript">
        $(function () {
            $(".datePicker").datepicker({ changeMonth: true, changeYear: true, yearRange: '-100:+0', dateFormat: 'dd/mm/yy', nextText: '>', prevText: '<', beforeShow: function () { $(".ui-datepicker").removeClass("ui-datepicker-enabled-select"); } });
        });
   </script>
   
  <% using (Html.BeginForm())
     { %>
  
         <table id="PageMainContent">
                <tr>
                    <th colspan="3">
                        <div style="width:100%">
                            Passport Verification Summary Report
                        </div>
                    </th>
                </tr>               
                <tr>
                    <td style="font-weight:bold; width: 170px;">
                        Date From
                    </td>
                    <td style="width: 480px">
                       <%: Html.TextBox("fromDate", Server.HtmlEncode(DateTime.Now.AddDays(-7).ToString("dd/MM/yyyy")), new { @class = "datePicker", style = "width:65px" })%>   
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold; width: 170px;">
                        Date To
                    </td>
                    <td style="width: 480px">
                       <%: Html.TextBox("toDate", Server.HtmlEncode(DateTime.Now.ToString("dd/MM/yyyy")), new { @class = "datePicker", style = "width:65px" })%>   
                    </td>
                </tr>                                                                        
                 <tr>
                    <td style="font-weight:bold; width:100%;" colspan="2">
                        <input type="submit" value="View" />            
                    </td>
                </tr> 
        </table>
    <%} %>
</asp:Content>