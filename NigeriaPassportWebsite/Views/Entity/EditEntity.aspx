﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Entity.OperatorEntity>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    System User Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "EditEntityForm" }))
   { %>
    <%: Html.ValidationSummary(true) %>
   
   <script type="text/javascript">
       $(function () {
           $(".datePicker").datepicker({ changeMonth: true, changeYear: true, yearRange: '-100:+0', dateFormat: 'dd/mm/yy' });           
       });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#save').click(function () {

                var divisionId = '<%: Model.DivisionID %>';
                var entityId = '<%: Model.EntityId %>';
                var emailaddress = $('#Email_Address').val();

                $.get('<%= Url.Action("VerifyEmailAddress","Entity") %>',
                    { emailaddress: emailaddress, divisionId: divisionId, entityId: entityId },
                    function (data) {
                        if (data.erroronsave != null &&
                            data.erroronsave.toString() == "true") {
                            $.get('<%= Url.Action("SystemError","Error") %>',
                                    null,
                                    function (data) {
                                        $('#dialogshowmessage').html(data);
                                        $('#dialogshowmessage').dialog('open');
                                    });
                        }
                        else if (data.inuse.toString() == "true") {
                            $('#dialogshowmessage').html(data.messageview);
                            $('#dialogshowmessage').dialog('open');
                        }
                        else {
                            $("#EditEntityForm").submit();
                        }
                    });
            });

            $("#dialogshowmessage").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                position: 'center'
            });

            $("#dialogsaved").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                position: 'center',
                buttons: {
                    "Continue": function () {
                        window.location.href = '<%: Url.Action("EntityHome","Entity") %>'
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        });

        $(document).ready(function () {
            var saved = '<%: ViewData["SuccessfulSave"] %>';
            
            if (saved != null &&
                saved.toString().toLowerCase() == 'true') {
                
                $('#dialogsaved').dialog('open');
            }
        });

        function CloseMessageDialog() {
            $('#dialogshowmessage').dialog('close');
        }
    </script>

   <table id="PageMainContent">        
        <tr>
            <th colspan="2">
                System User Details
            </th>
        </tr>
        <tr>
            <td colspan= "2">
                Please provide the following personal information
            </td>
        </tr>
        <tr>
            <td style="width:35%; font-weight:bold;">
                <%: Html.LabelFor(model => model.Firstname)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Firstname)%>
                <%: Html.ValidationMessageFor(model => model.Firstname)%>
            </td>
        </tr>
         <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.Surname)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Surname)%>
                <%: Html.ValidationMessageFor(model => model.Surname)%>
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.Middlename)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Middlename)%>
                <%: Html.ValidationMessageFor(model => model.Middlename)%>
            </td>
        </tr>        
        <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.DOB)%>
            </td>
            <td>
                <%:Html.TextBoxFor(model => model.DOB, new { @Value = Model.DOB.ToString("dd/MM/yyyy"), @class = "datePicker", style = "width:70px" })%><br />
                <%: Html.ValidationMessageFor(model => model.DOB)%>
            </td>
        </tr>
         <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.Gender)%>
            </td>
            <td> 
                <%: Html.DropDownListFor(model => model.Gender, (SelectList)ViewData["GenderList"])%><br />
                <%: Html.ValidationMessageFor(model => model.Gender)%>
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.Email_Address)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Email_Address)%>
                <%: Html.ValidationMessageFor(model => model.Email_Address)%>
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.GSM_Number)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.GSM_Number)%> i.e. +234 12 345 6789
                <%: Html.ValidationMessageFor(model => model.GSM_Number)%>
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold;">
                <%: Html.LabelFor(model => model.PassportVerification)%>
            </td>
            <td>
                <% if (Model.Administrator == true){%>
                    <%: Html.EditorFor(model => model.PassportVerification)%>
                <%}
                   else
                {%>
                    <%: Html.CheckBoxFor(model => model.PassportVerification, new { disabled = "disabled"})%>
                <%}%>
                <%: Html.ValidationMessageFor(model => model.PassportVerification)%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <%: Html.HiddenFor(model => model.EntityId) %>
                <%: Html.HiddenFor(model => model.DivisionID) %>
                <%: Html.HiddenFor(model => model.Administrator) %>
                <%: Html.HiddenFor(model => model.ReplacementEntityId) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <input type="button" value="Save" id="save" />
            </td>
        </tr>
        </table>
        <% } %>
    <div id="dialogshowmessage" title="Message">
    </div>
    <div id="dialogsaved" title="Save Successful">
        <p><span style="float:left; margin:0 0px 20px 0;"></span>The user has been saved successfully and the new PIN has been sent to the email address specified. Please click continue to go back to the division page or close to add another user.</p>
    </div>
</asp:Content>

