﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Divisions and Operators
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>
    <div style="padding-left:8px">
        <% foreach (var item in (List<PassportCommon.Models.Entity.LevelDivision>)ViewData["Breadcrumb"])
           { %>    
            <%: Html.ActionLink(item.Name, "ShowSubdivision", new { id = item.DivisionID })%>    
            <%if (item != ((List<PassportCommon.Models.Entity.LevelDivision>)ViewData["Breadcrumb"]).Last())
              {  %>
                -
            <%} %>
        <%} %>
    </div>
</h2>
    
 <table id="PageMainContent" class="ListPadding">
 <% if (((bool)ViewData["HideDivision"]) != true)
    {  %>
        <tr>
            <th colspan="2">
                Divisions                
            </th>
        </tr>
        <tr>
            <td>
                <% Html.RenderPartial("LevelSubDivisionView", ViewData["LevelSubDivision"]); %>               
            </td>
        </tr>
<% } %>
        <tr>
            <th colspan="2">
                
                   Administrators
                       </th>
        </tr>
        <tr>
            <td>
                <% ViewData["AddText"] = "Add Administrator"; ViewData["IsAdmin"] = true; %>                
                <% Html.RenderPartial("EntityBriefListView", ViewData["Administrators"]); %>
            </td>
        </tr>

        <tr>
            <th colspan="2">              
                   System Users                
            </th>
        </tr>
        <tr>
            <td>
                <% ViewData["AddText"] = "Add System User"; ViewData["IsAdmin"] = false;%>
                <% Html.RenderPartial("EntityBriefListView", ViewData["SystemUsers"]); %>
            </td>
        </tr>
</table>    
</asp:Content>
