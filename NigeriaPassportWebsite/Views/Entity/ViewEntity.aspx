﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Entity.OperatorEntity>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    System User Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h3>System User Details</h3>

  <table id="PageMainContent">
        <tr>
            <th colspan="3">
                <div style="width:100%">
                    Personal Data
                </div>
            </th>
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.Firstname)%>
            </td>
            <td style="width:480px">
                <%: Model.Firstname%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.Surname)%>
            </td>
            <td style="width:480px">
                <%: Model.Surname%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.Middlename)%>
            </td>
            <td style="width:480px">
                <%: Model.Middlename%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.DOB)%>
            </td>
            <td style="width:480px">
                <%: Model.DOB.ToString("dd/MM/yyyy")%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.Gender)%>
            </td>
            <td style="width:480px">
                <%: Model.Gender%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.Email_Address)%>
            </td>
            <td style="width:480px">
                <%: Model.Email_Address%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.GSM_Number)%>
            </td>
            <td style="width:480px">
                <%: Model.GSM_Number%>       
            </td>           
        </tr>
        <tr>
            <th colspan="3">
                <div style="width:100%">
                    Access Rights
                </div>
            </th>
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                Division Name
            </td>
            <td style="width:480px">
                <%: Model.DivisionName%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.Administrator)%>
            </td>
            <td style="width:480px">
                <%: Html.CheckBox("cb", Model.Administrator, new { disabled = "disabled" })%>       
            </td>           
        </tr>
        <tr>
            <td style="width:170px; font-weight:bold;">
                <%: Html.LabelFor(model => Model.PassportVerification)%>
            </td>
            <td style="width:480px">                
                <%: Html.CheckBox("cb", Model.PassportVerification, new { disabled = "disabled" })%>

                <% if (Model.Administrator)
                   { %>
                        <% if (Model.PassportVerification)
                           { %>
                                <%: Html.ActionLink("Remove Passport Verification", "RemovePassportVerification", "Entity", new { entityID = Model.EntityId }, new { @class = "actionlinkbutton" }) %>
                        <% }
                           else
                           {%>
                                <%: Html.ActionLink("Add Passport Verification", "AddPassportVerification", "Entity", new { entityID = Model.EntityId }, new { @class = "actionlinkbutton" }) %>
                        <%} %>
                <%} %>
            </td>           
        </tr>
    </table>
</asp:Content>

