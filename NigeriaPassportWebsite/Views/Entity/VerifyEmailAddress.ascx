﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Entity.EntityEmailReplace>" %>

<script type="text/javascript">
    function SubmitForm(replacemententityId) {
        $('#ReplacementEntityId').val(replacemententityId);
        $("#EditEntityForm").submit();     
    }
</script>

<table>
    <tr>
        <td>
            <%: Html.DisplayFor(m => m.Message) %><br />
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">
            <% if (Model.CanReplace) %>
            <% { %>
                <input type="button" value="Yes" onclick="SubmitForm(<%: Model.EntityIdUsedFor %>)" />
                &nbsp;
                <input type="button" value="No" onclick="CloseMessageDialog()" />
            <% } %>
            <% else %>
            <% { %>
                <input type="button" value="Close" onclick="CloseMessageDialog()" />
            <% } %>
        </td>
    </tr>
</table>