﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PassportCommon.Models.Entity.OperatorEntity>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Administrator Contact List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<table id="PageMainContent">
    <tr>
        <th colspan="4">
            <div style="width:100%">
                 Administrator Contact List
            </div>
        </th>
    </tr>
    <tr>
        <td>
            <table id="InnerContent">
                <tr>                
                    <th style="width: 200px;">                        
                            Firstname                        
                    </th>
                    <th style="width: 200px;">                        
                            Surname                        
                    </th>       
                    <th>            
                            Email Address
                    </th>        
                    <th>            
                            Mobile Number
                    </th>        
                </tr>
            <% foreach (var item in Model) { %>
                <tr>
                    <td>
                        <%: item.Firstname %>
                    </td>
                    <td>
                        <%: item.Surname %>
                    </td>
                    <td>
                        <%: item.Email_Address %>
                    </td>
                    <td>
                        <%: item.GSM_Number %>
                    </td>
                </tr>  
            <% } %>
            </table>
        </td>
    </tr>
    
</table>











</asp:Content>

