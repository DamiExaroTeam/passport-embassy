﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Entity.LevelDivision>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Division Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h3>Division Details</h3>


<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    
     <table id="PageEdit">       
        <tr>
            <td colspan= "2">
                Please provide the Division Name    
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                Division Name
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Name)%>
                <%: Html.ValidationMessageFor(model => model.Name)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                Division Class
            </td>
            <td> 
                 <%--<%=Html.DropDownList("DivisionClass", ViewData["DivisionClass"] as SelectList, new { style = "width: 250px;" })%>--%>
                <%=Html.DropDownListFor(model => model.DivisionClassID, (SelectList)ViewData["DivisionClass"],new { style = "width: 250px;" })%>
                
                <%--<%: Html.ValidationMessageFor(model => model.DivisionClassID)%>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <%: Html.HiddenFor(model => model.ParentDivisionID) %>                
                <%: Html.HiddenFor(model => model.DivisionID) %> 
            </td>
        </tr>        
        <tr>
            <td colspan="2" style="text-align:center">
                <input type="submit" value="Save" id="save" />
            </td>
        </tr>
        </table>
        <% } %>    
</asp:Content>

