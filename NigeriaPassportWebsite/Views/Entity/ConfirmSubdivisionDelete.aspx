﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<NigeriaPassportWebsite.Models.Entity.SubdivisionDelete>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Confirm Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<table id="PageMainContent">
        <tr>
            <th>
                Confirm Division Delete
            </th>
        </tr>   
        <tr>
            <td>
                You are about to remove the <%: Model.DivisionName %> subdivision.  This change is permanent.
            </td>
        </tr> 
        <tr>
            <td>
                <% if (Model.OperatorCount > 0)
                   { %>
                        There is <%: Model.OperatorCount%> operator(s) associated with this division.
                <% } %>
                <br />
                <br />
                Do you agree that this subdivision and all related operators will be removed?
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.HiddenFor(model => model.DivisionParentID) %>
                <%: Html.HiddenFor(model => model.DivisionID) %>

                <%: Html.ActionLink("Yes", "DeleteDivision", "Entity", new { divisionParentID = (int)Model.DivisionParentID, divisionID = (int)Model.DivisionID }, new { @class = "actionlinkbutton" })%>
                <%: Html.ActionLink("No", "ShowSubdivision", "Entity", new { id = (int)Model.DivisionParentID }, new { @class = "actionlinkbutton" })%>
            </td>
        </tr>
    </table>     
</asp:Content>
