﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<PassportCommon.Models.Entity.OperatorEntity>>" %>

<table cellpadding="0" cellspacing="0">
    <tr>                
        <td style="width: 200px;"> 
            <div class="ListHeader"> 
                Firstname                        
            </div>                       
        </td>
        <td style="width: 200px;">    
            <div class="ListHeader">                    
                Surname                        
            </div>
        </td>       
        <td>      
            <div class="ListHeader">      
                Status
            </div>
        </td>
        <td style="width: 150px;padding-left:10px;">            
            <div class="ListHeader">
                Passport Verification            
            </div>
        </td>
        <td>      
            <div class="ListHeader">      
                Action
            </div>
        </td>
        <td style="padding-left:10px;">   
            <div class="ListHeader">         
                View            
            </div>
        </td>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: item.Firstname %>
        </td>
        <td>
            <%: item.Surname %>
        </td>
        <td>
            <%: item.WebAccountStatusName.ToString() %>
        </td>
        <td style="text-align:center;">
            <%: Html.CheckBox("cb", item.PassportVerification, new { disabled = "disabled" })%>
        </td>
         <td>
            <% if((bool)ViewData["AllowChange"] == true) { %>
                <% if (item.WebAccountStatus == PassportCommon.Enumeration.WebAccount.WebAccountStatus.Active && Passport.Models.Session.SessionData.EntityId != item.EntityId)
                   { %>
                        <%: Html.ActionLink("Suspend", "SuspendEntity", new { id = item.EntityId, divisionID = item.DivisionID })%> 
                <%}%>
                <% else if (item.WebAccountStatus == PassportCommon.Enumeration.WebAccount.WebAccountStatus.Suspended && Passport.Models.Session.SessionData.EntityId != item.EntityId)
                   { %>
                        <%: Html.ActionLink("Activate", "ActivateEntity", new { id = item.EntityId, divisionID = item.DivisionID })%> or
                        <%: Html.ActionLink("Delete", "DeleteEntity", new { id = item.EntityId, divisionID = item.DivisionID })%>
                <%}%>
                <% else
                   { %>
                       None
                <%}%>                   
            <%} %>
            <%else { %>                
                None
            <%} %>            
        </td>
        <td style="padding-left:10px;">
            <%: Html.ActionLink("Details", "ViewEntity", new { id = item.EntityId })%> 
        </td>
    </tr>  
<% } %>

</table>

<p>
    <% if((bool)ViewData["AllowChange"] == true) { %>           
        <%: Html.ActionLink(ViewData["AddText"].ToString(), "AddEntity", new { divisionID = ViewData["ParentSubDivisionID"], isAdmin = ViewData["IsAdmin"] })%>
    <%} else { %>
        &nbsp
    <%} %>    
</p>

