﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<PassportCommon.Models.Entity.LevelDivision>>" %>

<% if(Model.Count() > 0) { %>
<table>
    <tr>        
        <td style="width: 400px;"> 
            <div class="ListHeader">           
                Description            
            </div>
        </td>
        <td style="width: 100px;text-align:center;">           
            <div class="ListHeader">
                User Count           
            </div>
        </td>
        <td></td> 
    </tr>

<% foreach (var item in Model) { %>
    <tr>                        
        <td>
            <%: Html.ActionLink( item.Name , "ShowSubdivision", new { id = item.DivisionID})%>            
        </td>
        <td style="text-align:center;">
            <%: item.UserCount %>
        </td>
        <td>
            <% if((bool)ViewData["AllowChange"] == true) { %>           
                <%: Html.ActionLink("Edit", "EditSubdivision", new {id=item.DivisionID})%> |            
                <%: Html.ActionLink("Delete", "DeleteSubdivision", new { id = item.DivisionID })%>
            <%} else { %>
                &nbsp
            <%} %>            
        </td>
    </tr>  
<% } %>

</table>
<%} else { %>
No divisions loaded
<%}%>
<p>
    <% if ((bool)ViewData["AllowChangeDivision"] == true)
       { %>           
        <%: Html.ActionLink("Add Division", "AddSubdivision", new { id = (int)ViewData["ParentSubDivisionID"] })%>
    <%} else { %>
        &nbsp
    <%} %>
</p>

