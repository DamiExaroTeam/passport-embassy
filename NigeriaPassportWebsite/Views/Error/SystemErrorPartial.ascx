﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<table>
    <tr>
        <th>
            Sorry for the inconvenience, but we have encoutered a problem.
        </th>        
    </tr>
    <tr>
        <td>
                <br />We have encountered a problem and could not continue.<br /> The problem has been logged and the software team will attend to the problem as soon as possible. <br /><br />Sorry for the inconvenience.
        </td>
    </tr>
</table>