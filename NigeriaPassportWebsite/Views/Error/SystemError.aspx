﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    oops...
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<span style="float:left;width:20px;">&nbsp;</span>    
    <table>
        <tr>
            <th>
                Sorry for the inconvenience, but we have encoutered a problem.
            </th>        
        </tr>
        <tr>
            <td>
                    <br />We have encountered a problem and could not continue.<br /> The problem has been logged and the software team will attend to the problem as soon as possible. <br /><br />Sorry for the inconvenience.
            </td>
        </tr>
    </table>   

</asp:Content>
