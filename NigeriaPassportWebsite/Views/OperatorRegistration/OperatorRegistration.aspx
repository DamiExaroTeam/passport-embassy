﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Passport.Models.Operator.OperatorRegistrationWeb>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Operator Registration
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h3>Operator Registration</h3>

<% Html.EnableClientValidation(); %>

<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "OperatorRegistrationForm" }))
   { %>
    <%: Html.ValidationSummary(true) %>

    <script type="text/javascript">
        $(function () {
            $(".datePicker").datepicker({ changeMonth: true, changeYear: true, yearRange: '-100:+0', dateFormat: 'dd/mm/yy' });
        });

    </script>
    
    <table id="PageMainContent">
        <tr>
            <th colspan="2">
                <div style="width:100%">
                    Personal Data
                </div>
            </th>
        </tr>
        <tr>
            <td colspan= "2">
                Please provide the following personal information
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.Surname)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Surname)%>
                <%: Html.ValidationMessageFor(model => model.Surname)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.Firstname) %>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Firstname)%>
                <%: Html.ValidationMessageFor(model => model.Firstname)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.Middlename)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.Middlename)%>
                <%: Html.ValidationMessageFor(model => model.Middlename)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.Gender)%>
            </td>
            <td> 
                <%: Html.DropDownListFor(model => model.Gender, (SelectList)ViewData["GenderList"])%><br />
                <%: Html.ValidationMessageFor(model => model.Gender)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.DOB)%>
            </td>
            <td style="width:500px">
                <%:Html.TextBoxFor(model => model.DOB, new { @Value = Model.DOB.ToString("dd/MM/yyyy"), @class = "datePicker", style = "width:70px" })%><br />
                <%: Html.ValidationMessageFor(model => model.DOB)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.GSM_Number)%>
            </td>
            <td> 
                <%: Html.EditorFor(model => model.GSM_Number)%> i.e. +234 12 345 6789
                <%: Html.ValidationMessageFor(model => model.GSM_Number)%>
            </td>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                <%: Html.LabelFor(model => model.Email_Address)%>
            </td>
            <td> 
                <%: Html.TextBoxFor(model => model.Email_Address, new { disabled = "disabled", @readonly = "readonly" })%>
                <%: Html.ValidationMessageFor(model => model.Email_Address)%>
                <%: Html.HiddenFor(model => model.Email_Address)%>
            </td>
        </tr>
        
        <% if (Model.WebAccountId <= 0) %>
        <% { %>
            <tr>
                <th colspan="2">
                    <div style="width:100%">
                        Login Data
                    </div>
                </th>
            </tr>
            <tr>
                <td colspan="2">
                    Please select a username and password that can be used to log in to your account. The password must have a minimum of 8 and maximum of 20 charaters and may not be "password". Please select a "strong" password with a combination of alpha and numeric characters and capital and small letters.
                    Please note the password is case sensitive.
                </td>
            </tr>
            <tr>
                <td style="width:15%; font-weight:bold;">
                    <%: Html.LabelFor(model => model.UserName)%>
                </td>
                <td> 
                    <%: Html.EditorFor(model => model.UserName)%>
                    <%: Html.ValidationMessageFor(model => model.UserName)%>
                </td>
            </tr>
            <tr>
                <td style="width:15%; font-weight:bold;">
                    <%: Html.LabelFor(model => model.Password)%>
                </td>
                <td> 
                    <%: Html.PasswordFor(model => model.Password, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.Password, string.Empty) })%>
                    <%: Html.ValidationMessageFor(model => model.Password)%>
                </td>
            </tr>
            <tr>
                <td style="width:15%; font-weight:bold;">
                    Retype Password
                </td>
                <td> 
                    <%: Html.PasswordFor(model => model.RetypePassword, new { value = PassportCommon.Utilities.GeneralUtilities.IsNullObjectOrIsEmptyString(Model, "") == "" ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.RetypePassword, string.Empty) })%>
                    <%: Html.ValidationMessageFor(model => model.RetypePassword)%>
                </td>
            </tr>   
        <% } %>
        <tr>
            <td colspan="2">
                <%: Html.HiddenFor(model => model.EntityId) %>
                <%: Html.HiddenFor(model => model.ValidationGUID) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <input type="submit" value="Continue >" id="save" />
            </td>
        </tr>
    </table>
<% } %>

</asp:Content>
