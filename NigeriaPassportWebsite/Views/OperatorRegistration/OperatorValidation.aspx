﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Operator.OperatorRegistration>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Operator Validation
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">
    jQuery(function ($) {
        $("#ValidationGUID").mask("********-****-****-****-************");
    });
</script>

<% Html.EnableClientValidation(); %>

<% using (Html.BeginForm(null, null, FormMethod.Post, new { id="OperatorValidation"})) { %>
    <%: Html.ValidationSummary(true) %>
    
    <table id="PageMainContent">
        <tr>
            <th colspan="3">
                <div style="width:100%">
                    Validate Your Registration Code
                </div>
            </th>
        </tr>
        <tr>
            <td>
                Please enter your registration code below
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.TextBoxFor(m => m.ValidationGUID, new { style="width:250px" }) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <input type="submit" value="Submit" id="save"/>
            </td>
        </tr> 
    </table>
<% } %>

</asp:Content>
