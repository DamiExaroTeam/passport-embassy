﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Testing Form
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <% using (Html.BeginForm())
    { %>

    Web Account ID <%: Html.TextBox("webAccountID")%> <br />    
    
    <select name="validationType">
        <option value="1">Activation</option>
        <option value="2">Lost Password</option>
        <option value="3">Lost Username</option>
    </select>
    <br />
    <br />

    <input type="submit" value="Submit" /><br />
    <br />

    <div>
        <% if (TempData["Result"] != null) %>
        <% { %>
             <% foreach (string code in ((Dictionary<long, string>)TempData["Result"]).Values)%>
             <% { %>
                    <%: Html.Label(code) %><br />
             <% } %>
        <% } %>
    </div>

<% } %>


</asp:Content>
