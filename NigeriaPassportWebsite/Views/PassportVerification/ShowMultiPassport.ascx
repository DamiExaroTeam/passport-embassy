﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Passport.MultiPassport>" %>

<table>
    <tr>
        <th>Associated Passports</th>
        <th>Passport Detail</th>    
    </tr>
    <tr>
        <td valign="top" style="padding-right:30px"><% Html.RenderPartial("PassportHistory", Model); %></td>
        <td valign="top" style="border-width:thin; border-color:Black; border-style:double;"><% Html.RenderPartial("PassportDetail", Model.Passport); %></td>
    </tr>    

</table>