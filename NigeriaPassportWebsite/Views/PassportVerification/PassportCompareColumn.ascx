﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Passport.Passport>" %>
<span>
    
<table style="width:150px;display:inline-table;vertical-align:top;" class="CompareList">                                
    <tr>                                    
        <td>
            <%  if (Model.DocType == "Standard Passport")
            {%>
                <span style="background-color:#4b5e58;display:inline-block;color:White;"><%: Model.DocType%></span>
            <%}
                else if (Model.DocType == "Diplomatic Passport")
            {%>
                <span style="background-color:#b72235;display:inline-block;color:White;"><%: Model.DocType%></span>
            <%}
                else if (Model.DocType == "Official Passport")            
              {%>
                <span style="background-color:#5badd5;display:inline-block;color:White;"><%: Model.DocType%></span>
            <%}%>
        </td>
    </tr>       
    <tr>                         
        <td>
            <b><%: Model.DocNo%></b>
        </td>
    </tr>
    <tr>                         
        <td>
            <%: Model.Surname%>        
        </td>
    </tr>
    <tr>                            
        <td>
            <%: Model.FirstName%>        
        </td>
    </tr>
    <tr>                            
        <td>
            <%: Model.OtherNames%> &nbsp       
        </td>
    </tr>
    <tr>                            
        <td>                                
            <%: Model.BirthDate.ToString("dd/MM/yyyy")%>                                
        </td>
    </tr>
    <tr>                            
        <td>
            <%: Model.GenderDesc%>        
        </td>
    </tr>    
    <tr>                          
        <td>
            <%: Model.IssueDate.ToString("dd/MM/yyyy")%>
        </td>
    </tr>
    <tr>                          
        <td>
            <%: Model.ExpiryDate.ToString("dd/MM/yyyy")%>
        </td>
    </tr>
    <tr>
        <td style="height:210px">
            <img src="<%:Url.Action("PassportImage", "Image", new { passportNo = Model.DocNo, isFaceImage = true }) %>" width="150px" height="200px" />        
        </td>
    </tr>
    <tr>
        <td style="height:40px">           
            <img src="<%:Url.Action("PassportImage", "Image", new { passportNo = Model.DocNo, isFaceImage = false }) %>" width="150px", height="33px" />                      
        </td>
    </tr>
    <tr>                            
        <td>
            <%: Model.IssuePlace%>
        </td>
    </tr>
     <tr>                            
        <td style="height:40px;white-space:normal;">
            <%  if (Model.StageCode == "EM5000" || Model.StageCode == "PM2000")
            {%>
                <span style="background-color:Green;display:inline-block;color:White;"><%: Model.StageDescription %></span>
            <%}
            else
            {%>
                <span style="background-color:Red;display:inline-block;color:White;"><%: Model.StageDescription %></span>
            <%}%>                              
        </td>
    </tr>
    <tr>
        <td>
            <%: Html.ActionLink("Print", "PassportValidation", "Report", new { docNo = Model.DocNo, surname = Model.Surname, firstName = Model.FirstName, gender = Model.Gender, birthDate = Model.BirthDate, stageDescription = Model.StageDescription, historyID = Model.HistoryID, otherNames = Model.OtherNames, issueDate = Model.IssueDate, expiryDate = Model.ExpiryDate, docType = Model.DocType, issuePlace = Model.IssuePlace }, new { target = "_blank" })%>        
        </td>
    </tr>
</table>

</span>