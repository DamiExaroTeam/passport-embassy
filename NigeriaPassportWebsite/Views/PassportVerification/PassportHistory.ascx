﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Passport.MultiPassport>" %>

<table>
    <tr>        
        <td style="font-weight:bold;">
            Passport Number
        </td>
        <td style="font-weight:bold;">
            Passport Date
        </td>        
    </tr>
<% foreach (var item in ((PassportCommon.Models.Passport.MultiPassport)Model).PassportHistory) { %>
    <tr>           
        <td>
            <% if (item.PassportNumber == Model.Passport.DocNo)
               {%>
                    <b><a href="javascript:ShowPassportDetail('<%:((PassportCommon.Models.Passport.MultiPassport)Model).IdentificationGuid%>','<%: item.PassportNumber %>')"><%: item.PassportNumber%></a></b>
             <%}
               else
               {%>
                    <a href="javascript:ShowPassportDetail('<%:((PassportCommon.Models.Passport.MultiPassport)Model).IdentificationGuid%>','<%: item.PassportNumber %>')"><%: item.PassportNumber%></a>
             <%} %>
        </td>        
        <td>
            <% if (item.PassportNumber == Model.Passport.DocNo)
               {%>
                    <b><%: item.PassportDateString %></b>
               <%}
               else
               {%>
                    <%: item.PassportDateString %>
             <%} %>
        </td>
    </tr>  
<% } %>
</table>

