﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Passport.Passport>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Verify Passport
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">
    function MultiFail() {
        document.location = 'MultiFailLogout';
    }
    
    function ShowPassportDetail(identificationGuid, passportNumber) {
        $.ajax({
            type: 'GET',
            url: "/PassportVerification/ShowPassport",
            data: { identificationGuid: identificationGuid, passportNumber: passportNumber},
            dataType: "json",
            success: function (json) {
                $("#ResultDiv").html(json);
            },
            error: function (x, y, z) {
                alert(x.responseText);
            }
        });        
    }

</script>

    <% using (Ajax.BeginForm("VerifyPassport", new AjaxOptions() { UpdateTargetId = "ResultDiv", InsertionMode = InsertionMode.Replace, HttpMethod = "POST", LoadingElementId = "loadingDiv" }))
	   { %>
            <%: Html.ValidationSummary() %>

            <div id="FixedWidthBox">
            <table id="PageMainContent">
                <tr>
                    <th colspan="3" style="text-align:left;padding-left:260px;">                                               
                            Search Passport Information                        
                    </th>
                </tr>               
                <tr>
                    <td colspan="3">
                        <div style="width:100%">                            
                             Please note, this is an exact search. It will only give results if the exact passport number and surname is used.
                            <br />Verifications are tracked and your organisation will be charged for each transaction.
                            <br /><br />You indicate that you agree with the conditions stated by clicking the Search button.<br />&nbsp
                            
                        </div>
                    </td>
                </tr>               
                <tr>
                    <td style="width:200px; font-weight:bold;">
                        <%: Html.LabelFor(model => model.DocNo)%> 
                    </td>
                    <td>
                        <%: Html.EditorFor(model => model.DocNo)%><br />
                        <%: Html.ValidationMessageFor(model => model.DocNo)%>
                    </td>           
                </tr>
                <tr>
                    <td style="font-weight:bold;">
                        <%: Html.LabelFor(model => model.Surname)%> 
                    </td>
                    <td>
                        <%: Html.EditorFor(model => model.Surname)%><br />
                        <%: Html.ValidationMessageFor(model => model.Surname)%>
                    </td>           
                </tr>
                <tr>
                    <td style="font-weight:bold;">
                        Search All Passport Types <br />
                        <p class="SmallText">Please note: This search will take considerably longer due to a more complex retrieval of information.</p>
                    </td>
                    <td style="vertical-align:top;">
                        <%: Html.CheckBox("AllTypes",false)%>
                    </td>           
                </tr>               
                <tr>
                    <td>&nbsp</td>
                    <td style="font-weight:bold;">
                        <input type="submit" value="Search" />  
                        <br />          
                        <br />
                    </td>
                </tr> 
                 <tr>
                     <th colspan="3" style="text-align:left;padding-left:270px;">                       
                            Passport Search Results                       
                    </th>
                </tr>    
                <tr>
                    <td colspan="4">
                        <div id="loadingDiv" style="display:none">                            
                            <img src="../../Content/Images/ajax-loader.gif" alt="Loading" />
                        </div>
                        <div id="ResultDiv" style="width:100%;" ></div>
                    </td>
                </tr>               
            </table>
            </div>
    <% } %>	
</asp:Content>
