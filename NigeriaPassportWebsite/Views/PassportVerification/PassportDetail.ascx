﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Passport.Passport>" %>

 <%  if (Model == null)
                 {%>
                No Results Found                
           <%} else { %>      
           
           <table>
            <tr>
                <td valign="top">
                    <table>                        
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.DocNo)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.DocNo%>        
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.HistoryID)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.HistoryID%>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.IssueDate)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.IssueDate.ToString("dd/MM/yyyy")%>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.ExpiryDate)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.ExpiryDate.ToString("dd/MM/yyyy")%>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.DocType)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.DocType%>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.IssuePlace)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.IssuePlace%>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.StageDescription)%>
                            </td>
                            <td style="width: 480px">
                                <%  if (Model.StageCode == "EM5000" || Model.StageCode == "PM2000")
                                {%>
                                    <span style="background-color:Green;display:inline-block;color:White; width:70px;"><%: Model.StageDescription %></span>
                                <%}
                                else
                                {%>
                                    <span style="background-color:Red;display:inline-block;color:White; width:250px;"><%: Model.StageDescription %></span>
                                <%}%>                              
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.Surname)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.Surname%>        
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.FirstName)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.FirstName%>        
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.OtherNames)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.OtherNames%>        
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.GenderDesc)%>
                            </td>
                            <td style="width: 480px">
                                <%: Model.GenderDesc%>        
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; width: 170px;">
                                <%: Html.LabelFor(model => model.BirthDate)%>
                            </td>
                            <td style="width: 480px">                                
                                <%: Model.BirthDate.ToString("dd/MM/yyyy")%>                                
                            </td>
                        </tr>
                    </table>                
                </td>
                <td>
                    <img src="<%:Url.Action("PassportImage", "Image", new { passportNo = Model.DocNo, isFaceImage = true }) %>" height="220px" />
                    <br />
                    <img src="<%:Url.Action("PassportImage", "Image", new { passportNo = Model.DocNo, isFaceImage = false }) %>" width="165px" />   
                </td>
            </tr>           
           </table> 
            
        <%--<% if (Nibss.Models.Session.SessionData.AdditionalPrivilegeNames.Contains("Print Passport")){%> --%>
                    <%: Html.ActionLink("Print", "PassportValidation", "Report", new { docNo = Model.DocNo, surname = Model.Surname, firstName = Model.FirstName, gender = Model.Gender, birthDate = Model.BirthDate, stageDescription = Model.StageDescription, historyID = Model.HistoryID, otherNames = Model.OtherNames, issueDate = Model.IssueDate, expiryDate = Model.ExpiryDate, docType = Model.DocType, issuePlace = Model.IssuePlace }, null)%>
        <%--<% } %>--%>
<% } %>