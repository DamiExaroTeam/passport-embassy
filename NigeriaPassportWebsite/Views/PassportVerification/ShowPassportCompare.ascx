﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Passport.MultiPassport>" %>
  
  <table>
    <tr>
        <td>
            <b>Authorisation Code</b> : <%: Model.Passport.HistoryID%>      
        </td>
    </tr>
    <tr>
        <td>
            The first document is the one that was searched for. <br />The other passports belonging to this entity is arranged by issue date.
        </td>
    </tr>
    <tr>
        <td valign="top" style="vertical-align:top;overflow:hidden;white-space:nowrap;">            
                <% Html.RenderPartial("PassportCompareheaders"); %>
                <% foreach(PassportCommon.Models.Passport.Passport pPort in Model.FullPassportHistoryList){ %>
                    <% Html.RenderPartial("PassportCompareColumn", pPort); %>
                <%} %>                    
        </td>
    </tr>
  </table>
  


