﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Passport.Models.Security.SecurityQuestionsWeb>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Change Security Questions
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {
            $('#SEC_Q_ID_1').change(function () {
                var securityquestion1 = $(this).val();
                
                $("#USER_ANSWER_1").val('');
                $("#USER_ANSWER_2").val('');
                $("#SEC_Q_ID_2").empty();

                $.getJSON('<%:Url.Action("GetFilteredSecurityQuestions","Security") %>',
                        { securityquestion1: securityquestion1 },
                        LoadQuestionsFiltered);
            });
        });

        $(document).ready(function () {
            $('#SEC_Q_ID_2').change(function () {
                $("#USER_ANSWER_2").val('');
            });
        });

        function LoadQuestionsFiltered(data) {

            var items = "";
            items += "<option selected=\"selected\" value='-1'>(Please Select)</option>";
            $.each(data, function (i, item) {
                items += "<option value='" + item.Value + "'>" + item.Text + "</option>";
            });
            $("#SEC_Q_ID_2").html(items);           
        }
    </script>

<h3>Change Security Questions</h3>

<% using (Html.BeginForm("ChangeSecurityQuestions", "Security", FormMethod.Post, new { id = "ChangeSecurityQuestions" }))
{ %>
    <table id="PageMainContent">
        <tr>
            <th colspan="2">
                <div style="width:100%">
                    Security Questions
                </div>
            </th>
        </tr>
        <tr>
            <td colspan="2">
                In order for us to identify you in the future, we require that you select any of the security questions from the drop down lists below and provide us with answers that you will remember. <br /><br />

                Please click on the arrow point downwards on the right of the boxes below to see the complete list of questions.<br /><br />
            </td>
        </tr>
        <tr>
            <td style="width:300px;">
                <%: Html.DropDownListFor(model => model.SEC_Q_ID_1, (SelectList)Model.SecurityQuestions1) %>
                <%: Html.HiddenFor(model => model.SEC_Q_ID_1_OLD) %>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.USER_ANSWER_1) %>
            </td>
        </tr>
        <tr>
            <td style="width:300px;">
                <%: Html.DropDownListFor(model => model.SEC_Q_ID_2, (SelectList)Model.SecurityQuestions2) %>
                <%: Html.HiddenFor(model => model.SEC_Q_ID_2_OLD) %>
            </td>
            <td>
                <%: Html.TextBoxFor(model => model.USER_ANSWER_2) %>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <br />
                <input type="submit" value="Save" />  
            </td>
        </tr>
     </table>
<% } %>

</asp:Content>
