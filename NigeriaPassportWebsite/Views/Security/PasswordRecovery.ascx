﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

    <table cellpadding="5px">
        <tr>
            <td>
                <h3>Contact Us</h3>
            </td>
        </tr>   
        <tr>
            <td>
                Phone: +234 (0)1 629 0842
            </td>
        </tr> 
        <tr>
            <td>
                <h3>User Recovery</h3>
            </td>
        </tr>   
        <tr>
            <td style="line-height: 30px;">
                Recover my <%: Html.ActionLink("Username", "LostUsername", "Security")%>
            </td>
        </tr> 
        <tr>
            <td style="line-height: 30px;">
                Recover my <%: Html.ActionLink("Password","LostPassword","Security") %>
            </td>
        </tr>
        <tr>
            <td style="line-height: 30px;">
                Recover both my <%: Html.ActionLink("username and password", "ResetLostUserLoginDetailsUser", "Security") %>
            </td>
        </tr>
    </table>     

