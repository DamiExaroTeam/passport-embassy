﻿<%@ Page Title="Recover Username" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Security.LostUserName>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Recover my Username
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
     <table id="PageMainContent">
        <tr>
            <th colspan="2">                
                Username Recovery             
            </th>                    
        </tr>
        <tr>
            <td>
                <table id="InnerContent">
                    <tr>
                        <td colspan="2">
                            Please fill in all the fields below to reset your Username.<br /><br />
                        </td>
                    </tr>                   
                    <tr>
                        <td style="border: none; width:200px;" valign="top">
                            What is your email address?
                        </td>
                        <td> 
                            <%: Html.EditorFor(model => model.EmailAddress) %><br />
                            <%: Html.ValidationMessageFor(model => model.EmailAddress)%>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none; width:200px;" valign="top">
                            What is your Password?
                        </td>
                        <td>            
                            <%: Html.PasswordFor(model => model.Password) %><br />
                            <%: Html.ValidationMessageFor(model => model.Password)%>
                        </td>
                    </tr>                  
                    <tr>
                        <td>
                            &nbsp
                        </td>
                        <td>            
                            <br />
                            <input type="submit" value="Submit" />             
                        </td>
                    </tr>               
                </table>
            </td>        
        </tr>
     </table>
<% } %>

</asp:Content>