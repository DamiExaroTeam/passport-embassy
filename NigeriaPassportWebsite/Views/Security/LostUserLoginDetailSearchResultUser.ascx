﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NigeriaPassportWebsite.Models.Security.LostLoginDetailsResetUser>" %>

<div id="dialog-confirm">
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#resetlogindetail').click(function () {

            var entityId = '<%: Model.EntityId %>';
            var displaysecurityquestionnumber = $('#DisplaySecurityQuestionNumber').val();
            var inputanswer = $('#Answer').val();

            $.post('<%= Url.Action("VerifySecurityQuestionAnswer","Security") %>',
                    { entityId: entityId, displaysecurityquestionnumber: displaysecurityquestionnumber, inputanswer: inputanswer },
                    function (data) {
                        if (data.reseterror != null &&
                            data.reseterror.toString() == "true") {
                            window.location.href = '<%: Url.Action("SystemError","Error") %>';
                        }
                        else if (data.resetlimitreached != null &&
                                 data.resetlimitreached.toString() == "true") {
                            $('#dialogresetlimitreached').html(data.message);
                            $('#dialogresetlimitreached').dialog('open');
                        }
                        else if (data.verificationerror != null &&
                                 data.verificationerror.toString() == "true") {
                            $('#dialogfailedmessage').html(data.message);
                            $('#dialogfailedmessage').dialog('open');
                        }
                        else if (data.resetsuccess != null &&
                                 data.resetsuccess.toString() == "true") {
                            $('#dialogresetsuccess').html(data.message);
                            $('#dialogresetsuccess').dialog('open');
                        }
                    });
        });

        $("#dialogfailedmessage").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            position: 'center',
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                }
            }
        });

        $("#dialogresetsuccess").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            position: 'center',
            close: function () {
                window.location.href = '<%: Url.Action("EntityHome","Entity") %>'
            },
            buttons: {
                "Continue": function () {
                    window.location.href = '<%: Url.Action("EntityHome","Entity") %>'
                }
            }
        });

        $("#dialogresetlimitreached").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            position: 'center',
            close: function () {
                window.location.href = '<%: Url.Action("Index","Home") %>'
            },
             buttons: {
                 "Close": function () {
                     window.location.href = '<%: Url.Action("Index","Home") %>'
                }
            }
         });
    });

    function ViewNextQuestion() {

        var displaynumber = parseInt($('#DisplaySecurityQuestionNumber').val());
        $('#Answer').val('');

        if (displaynumber % 2 === 0) {
            $('#divsecurityquestion1').show();
            $('#divsecurityquestion2').hide();
        }
        else {
            $('#divsecurityquestion1').hide();
            $('#divsecurityquestion2').show();
        }

        $('#DisplaySecurityQuestionNumber').val(displaynumber + 1);
    }
</script>

<% using (Html.BeginForm("LostUserLoginDetailSearchResult", "Security", FormMethod.Post, new { id = "LostUserLoginDetailSearchResult" }))
{ %>
    
    <% if (!Model.Verified) %>
    <% { %>
        <p>
            No user has been found with these details.
        </p>
    <% } %>
    <% else if (Model.ReachedIncorrectAnswerLimit) %>
    <% { %>
        <p>The limit for incorrect security question answers has been reached and this account has been suspended. Please contact the administrator.</p>
    <% } %>
    <% else if (Model.WebAccountStatus != PassportCommon.Enumeration.WebAccount.WebAccountStatus.Active) %>
    <% { %>
        <p>This account is not active, login detail recovery can only be done on active web accounts. Please contact the administrator.</p>
    <% } %>
    <% else if (!Model.SecurityQuestionsExist) %>
    <% { %>
        <p>No security questions exist for this account, please contact the administrator.</p>
    <% } %>
    <% else if (Model.SessionExpired) %>
    <% { %>
        <p>The session is has expired, please search for email user details again.</p>
    <% } %>
    <% else %>
    <% { %>

        <table style="width:100%">
            <tr>
                <td colspan="2">
                    Please answer the security questions that you selected with the first login on this website.
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight:bold;">
                    <div id="divsecurityquestion1">
                        <%: Html.DisplayFor(model => model.SecurityQuestion1)%>
                        &nbsp;
                        <a href="javascript:ViewNextQuestion()">View Next Question</a>
                    </div>
                    <div id="divsecurityquestion2" style="display:none">
                        <%: Html.DisplayFor(model => model.SecurityQuestion2)%>
                        &nbsp;
                        <a href="javascript:ViewNextQuestion()">View Next Question</a>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight:bold;">
                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    Answer: <%: Html.TextBoxFor(model => model.Answer)%>
                    <br />
                </td>
            </tr> 
            <tr>
                <td colspan="2" style="text-align:center;">
                    <input type="button" value="Reset Login Detail" id="resetlogindetail" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <%: Html.HiddenFor(model => model.EntityId)%>
                    <%: Html.HiddenFor(model => model.Email)%>
                    <%: Html.HiddenFor(model => model.Verified)%>
                    <%: Html.HiddenFor(model => model.SessionExpired)%>
                    <%: Html.HiddenFor(model => model.DisplaySecurityQuestionNumber)%>
                </td>
            </tr>
        </table>

        <div id="dialogfailedmessage" title="Verification Failed">
        </div>
        <div id="dialogresetsuccess" title="Login Detail Reset Successful">
        </div>
        <div id="dialogresetlimitreached" title="Reset Limit Reached">
        </div>
    <% } %>
<% } %>