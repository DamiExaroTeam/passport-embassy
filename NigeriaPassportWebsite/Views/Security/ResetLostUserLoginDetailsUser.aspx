﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Security.LostLoginUserDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ResetLostUserLoginDetails
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h3>Reset Lost User Login Details</h3>

<script type="text/javascript">
    $(document).ready(function () {
        $('#searchuser').click(function () {
            
            $.get('<%= Url.Action("LostUserLoginDetailSearchResultUser","Security") %>',
                $("#ResetLostUserLoginDetails").serialize(),
                function (data) {
                    $('#SecurityQuestionDetail').html(data);
                });
        });
    });

    </script>

<% using (Html.BeginForm("ResetLostUserLoginDetails", "Security", FormMethod.Get, new { id = "ResetLostUserLoginDetails" }))
{ %>
    <table id="PageMainContent">
        <tr>
            <th colspan="2">
                <div style="width:100%">
                    Login Detail Recovery
                </div>
            </th>
        </tr>
        <tr>
            <td style="width:15%; font-weight:bold;">
                Email Address
            </td>
            <td>
                <%: Html.EditorFor(model => model.Email_Address) %>
                <%: Html.ValidationMessageFor(model => model.Email_Address)%>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input type="button" value="Search" id="searchuser" />
                <br />
                <br />
            </td>
        </tr>
     </table>
<% } %>

    <div id="SecurityQuestionDetail">
    </div>
</asp:Content>
