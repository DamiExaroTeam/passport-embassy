﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<PassportCommon.Models.Security.LostLoginDetailsReset>>" %>

<script type="text/javascript">
    function SelectUser(entityId) {        
        $.get('<%= Url.Action("SelectLostLoginDetailUser","Security") %>',
            { entityId: entityId},
            function (data) {
                $('#SecurityQuestionDetail').html(data);
            });
    };
</script>

<table>
    <tr>
        <th>
            First Name
        </th>
        <th>
            Surname
        </th>
        <th>
            Mobile Number
        </th>
        <th>
            Email Address
        </th>
        <th>
        </th>
        <th>
        </th>
    </tr>

<% foreach (var item in Model) { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.FirstName) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Surname) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.ExistingMobileNumber) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.ExistingEmail) %>
            </td>
            <td>
                <%: Html.HiddenFor(modelItem => item.Verified) %>
                <%: Html.HiddenFor(modelItem => item.EntityId) %>
                <%: Html.HiddenFor(modelItem => item.WebAccountId) %>
                <%: Html.HiddenFor(modelItem => item.SecurityQuestion1) %>
                <%: Html.HiddenFor(modelItem => item.SecurityQuestionAnswer1) %>
                <%: Html.HiddenFor(modelItem => item.SecurityQuestion2) %>
                <%: Html.HiddenFor(modelItem => item.SecurityQuestionAnswer2) %>
                <%: Html.HiddenFor(modelItem => item.ResetUsername) %>
                <%: Html.HiddenFor(modelItem => item.NewMobileNumber) %>
                <%: Html.HiddenFor(modelItem => item.ShowMessage) %>
                <%: Html.HiddenFor(modelItem => item.Message) %>
                <%: Html.HiddenFor(modelItem => item.DOB) %>
            </td>
            <td>
                <input type="button" value="Select" onclick="SelectUser(<%: item.EntityId %>)"/>
            </td>
        </tr>
    
<% } %>

</table>
