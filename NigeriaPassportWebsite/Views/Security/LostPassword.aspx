﻿<%@ Page Title="Recover Password" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Security.LostPassword>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Recover my Password
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm()) { %>    
    <%: Html.ValidationSummary(true) %>
     <table id="PageMainContent">
        <tr>
            <th colspan="2">                
                Password Recovery
            </th>                    
        </tr>
        <tr>
            <td>
                <table id="InnerContent">
                <tr>
                    <td colspan="2">
                        Please fill in all the fields below to reset your password.<br /><br />
                    </td>
                </tr>                   
                <tr>
                    <td style="border: none; width:200px;" valign="top">
                        What is your email address?
                    </td>
                    <td> 
                        <%: Html.EditorFor(model => model.EmailAddress) %><br />
                        <%: Html.ValidationMessageFor(model => model.EmailAddress)%>
                    </td>
                </tr>
                <tr>
                    <td style="border: none; width:200px;" valign="top">
                        What is your username?
                    </td>
                    <td>            
                        <%: Html.EditorFor(model => model.Username) %><br />
                        <%: Html.ValidationMessageFor(model => model.Username)%>
                    </td>
                </tr>                  
                <tr>
                    <td>
                        &nbsp
                    </td>
                    <td>            
                        <br />
                        <input type="submit" value="Submit" />             
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        
 
    </table>
<% } %>

</asp:Content>

