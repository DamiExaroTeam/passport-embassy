﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"  Inherits="System.Web.Mvc.ViewPage<Passport.Models.Security.ChangePassword>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Change my Password
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm("ChangePassword", "Security", FormMethod.Post, new { id = "ChangeUserPassword" }))
   { %>    
        <%: Html.ValidationSummary(true) %>
        <table id="PageMainContent">
            <tr>
                <th colspan="2">
                    <div>
                        Change Password
                    </div>
                </th>                    
            </tr>
                <tr>
                <td colspan="2">
                    Please select your new password and key in the security code that was sent to you by email.<br /><br />
                </td>
            </tr>
            <tr>
                <td style="border: none; width:200px;" valign="top">
                    Security Code
                </td>
                <td>
                        <%: Html.EditorFor(model => model.SecurityCode) %> <%: Html.ActionLink("Resend Code", "ResendPasswordCode", new { id = Model.WebAccountId })%> <br />
                    <%: Html.ValidationMessageFor(model => model.SecurityCode)%>
                </td>      
            </tr>
            <tr>
                <td valign="top">
                    Please type your new password
                </td>
                <td>
                        <%: Html.PasswordFor(model => model.NewPassword) %><br />
                    <%: Html.ValidationMessageFor(model => model.NewPassword)%>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    Please type your new password again
                </td>
                <td>            
                        <%: Html.PasswordFor(model => model.NewPasswordRetype)%><br />
                    <%: Html.ValidationMessageFor(model => model.NewPasswordRetype)%>
                </td>
            </tr>  
            <tr>
                <td colspan="2">
                    <%: Html.HiddenFor(m => m.WebAccountId) %>
                </td>
            </tr>                
            <tr>
                <td>
                    &nbsp
                </td>
                <td>            
                    <br />
                    <input type="submit" value="Submit" />             
                </td>
            </tr>
 
        </table>
<% } %>

</asp:Content>

