﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Passport.Models.Security.ChangeUsername>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Change my Username
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm("ChangeUsername", "Security", FormMethod.Post, new { id = "ChangeUsername" }))
   { %>
        <%: Html.ValidationSummary(true) %>
        <table id="PageMainContent">
            <tr>
                <th colspan="2">
                    <div>
                        Change Username
                    </div>
                </th>                    
            </tr>
            <tr>
                <td>
                    <table id="InnerContent">
                        <tr>
                            <td colspan="2">
                                Please select your new Username and key in the security code that was sent to you by email.<br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="border: none; width:200px;" valign="top">
                                Security Code
                            </td>
                            <td>
                                <%: Html.EditorFor(model => model.SecurityCode) %>
                                <%: Html.ActionLink("Resend Code", "ResendUserCode", new { id = Model.WebAccountId })%>
                                <br />
                                <%: Html.ValidationMessageFor(model => model.SecurityCode)%>
                            </td>      
                        </tr>
                        <tr>
                            <td valign="top">
                                Please type your new username
                            </td>
                            <td>
                                    <%: Html.EditorFor(model => model.NewUserName) %><br />
                                <%: Html.ValidationMessageFor(model => model.NewUserName)%>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Please type your new username again
                            </td>
                            <td>            
                                <%: Html.EditorFor(model => model.NewUserNameRetype)%><br />
                                <%: Html.ValidationMessageFor(model => model.NewUserNameRetype)%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%: Html.HiddenFor(m => m.WebAccountId) %>
                            </td>
                        </tr>                
                        <tr>
                            <td>
                                &nbsp
                            </td>
                            <td>            
                                <br />
                                <input type="submit" value="Submit" />             
                            </td>
                        </tr>                    
                    </table>
                </td>
            </tr>
        </table>
<% } %>

</asp:Content>