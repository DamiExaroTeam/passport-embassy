﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Security.UserLoginDetailsChange>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Authentication for user details change
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <% using (Html.BeginForm()) { %>
                    
    <table id="PageMainContent" style="width:100%;">
        <tr>
            <th colspan="2">
            <div style="width:100%;">
                <% switch (Model.ChangeType) %>
                <% { %>
                <%       case (PassportCommon.Enumeration.Security.UserDetailsChangeType.UserName): %>
                            Username change
                <%          break; %>
                <%       case (PassportCommon.Enumeration.Security.UserDetailsChangeType.Password): %>
                            Password Change
                <%          break; %>
                <%       case (PassportCommon.Enumeration.Security.UserDetailsChangeType.Email): %>
                            Email Address Change
                <%          break; %>
                <% } %>
            </div>                           
            </th>                 
        </tr>
        <tr>
            <td colspan="2">
                <div style="width:100%;">
                <% switch (Model.ChangeType) %>
                <% { %>
                <%       case (PassportCommon.Enumeration.Security.UserDetailsChangeType.UserName): %>
                            Please re-enter your current username and password to proceed with the changing of your user name. Upon this confirmation you will receive a code via email that you will need to enter on the next page.
                <%          break; %>
                <%       case (PassportCommon.Enumeration.Security.UserDetailsChangeType.Password): %>
                            Please re-enter your current username and password to proceed with the changing of your password. Upon this confirmation you will receive a code via email that you will need to enter on the next page.
                <%          break; %>
                <%       case (PassportCommon.Enumeration.Security.UserDetailsChangeType.Email): %>
                            Please re-enter your current username and password to proceed with the changing of your email address.
                <%          break; %>
                <% } %>
            </div>   
                <br /><br />
            </td>                         
        </tr>
        <tr>
            <td style="width:100px;" >
                Username
            </td>
            <td style="width:550px">
                <%: Html.EditorFor(model => model.UserName)%><br />
                <%: Html.ValidationMessageFor(model => model.UserName)%>
            </td>      
        </tr>
        <tr>
            <td style="width:100px;">
                Password
            </td>
            <td style="width:550px">
                <%: Html.PasswordFor(model => model.Password, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.Password, string.Empty) })%><br />
                <%: Html.ValidationMessageFor(model => model.Password) %>
            </td>      
        </tr>
        <tr>
            <td style="width:100px;">
                <%: Html.HiddenFor(m => m.ChangeType) %>
            </td>
            <td style="width:550px"></td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <input type="submit" value="Submit" />                              
            </td>      
        </tr>
    </table>            
<% } %>

</asp:Content>
