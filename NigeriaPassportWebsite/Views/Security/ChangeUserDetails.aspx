﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Passport.Models.Security.ChangeUserDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Change Mobile number
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm("ChangeUserDetails", "Security", FormMethod.Post, new { id = "ChangeUserDetails" }))
   { %>
    <%: Html.ValidationSummary(true) %>     
        <table id="PageMainContent">
            <tr>
                <th colspan="2">
                    <div style="width:100%">
                        Change email address
                    </div>
                </th>                    
            </tr>
            <tr>
                <td style="width:170px;">
                    <%: Html.LabelFor(model => model.NewEmail)%>
                </td>
                <td>
                    <%: Html.EditorFor(model => model.NewEmail)%>
                    <%: Html.ValidationMessageFor(model => model.NewEmail)%>
                </td>
            </tr>

            <tr>
                <td style="width:170px;">
                    <%: Html.LabelFor(model => model.RetypeNewEmail)%>
                </td>
                <td>
                    <%: Html.EditorFor(model => model.RetypeNewEmail)%>
                    <%: Html.ValidationMessageFor(model => model.RetypeNewEmail)%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <input type="submit" value="Submit" />                              
                </td>      
            </tr>
        </table>
            
<% } %>

</asp:Content>

