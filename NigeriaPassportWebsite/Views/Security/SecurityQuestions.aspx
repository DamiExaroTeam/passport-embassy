﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<PassportCommon.Models.WebAccount.SecurityQuestion>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Security Questions
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">

    $(document).ready(function () {
        $('#Question1').change(function () { LoadQuestionsLeft(); });

        //For initial fill
        LoadQuestionsLeft();
    });

    function LoadQuestionsLeft() {

        var question1Selection = $("select[id$='Question1'] :selected").val();

        $('#Question2 >option').remove();

        $.ajax({
            type: 'GET',
            url: "/Security/GetSecurityQuestions",
            data: { selectedQuestion: question1Selection },
            dataType: "json",
            success: function (json) {

                $('#Question2Answer').val('');

                $('#Question2').append($('<option></option>').val('(Please Select)').html('(Please Select)'));

                $.each(json, function (id, dataObject) {
                    $('#Question2').append($('<option></option>').val(dataObject.Value).html(dataObject.Text));
                });
            },
            error: function (x, y, z) {
                alert("Error retrieving security questions from the server.");
            }
        });
    }
</script>


 <% using (Html.BeginForm()) { %>

 <table id="PageMainContent">
    <tr>
        <th colspan="2">
            <div>
                Security Questions 
            </div>
        </th>                    
    </tr>
    <tr>
        <td colspan="2">
            In order for us to identify you in the future, we require that you select any of the security questions from the drop down lists below and provide us with answers that you will remember. <br /><br />

            Please click on the arrow point downwards on the right of the boxes below to see the complete list of questions.<br /><br />
        </td>
    </tr>
    <tr>
        <td style="border: none; width:300px;" >
            <%: Html.DropDownList("Question1", (IEnumerable<SelectListItem>) ViewData["SecurityQuestions"])%>
        </td>
        <td>
            <%: Html.TextBox("Question1Answer")%>                         
        </td>      
    </tr>
        <tr>
        <td>
            <%: Html.DropDownList("Question2", (IEnumerable<SelectListItem>)ViewData["SecurityQuestions"])%>
        </td>
        <td>
            <%: Html.TextBox("Question2Answer")%>                         
        </td>      
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <input type="submit" value="Submit" />  
        </td>
    </tr>
</table>
<% } %>

</asp:Content>
