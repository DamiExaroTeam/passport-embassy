﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Passport.Models.Security.LoginDetailsChangeWeb>" %>
<%@ Import Namespace = "PassportCommon.Enumeration.WebAccount" %>


<script type="text/javascript">
    $(document).ready(function () {
        $('#save').click(function () {

            if ($('#showmessage') != null) {
                $('#showmessage').html('');
            }

            $.post('<%= Url.Action("ChangeLoginDetails","Security") %>',
                                        $("#ChangeLoginDetails").serialize(),
                                        function (data) {
                                            $('#dialogchangelogindetails').html(data);
                                        });
        });
    });

    function CloseDialog() {
        $('#dialogchangelogindetails').dialog('close');
    }
</script>

<% using (Html.BeginForm("ChangeLoginDetails", "Security", FormMethod.Post, new { id = "ChangeLoginDetails" }))
{ %>
    <% Html.EnableClientValidation(); %>
    <table>
        
        <% if (Model.ChangeSuccessful) %>
        <% { %>
            <tr>
                <td>
                    Your user details have been updated successfully, please use your new details the next time you log in.
                </td>
            </tr>
            <tr>
               <td colspan="2" style="text-align:center">
                   <input type="button" value="Close" onclick="CloseDialog()" />
               </td>
           </tr>
        <% } %>
        <% else %>
        <% { %>
            <tr>
                <td colspan="2">
                    <% switch (Model.LoginDetailStatus) %>
                    <% { %>
                        <% case LoginDetailStatus.Change_username_on_next_login: %>
                                You are required to change your username.
                        <%      break; %>
                        <% case LoginDetailStatus.Change_password_on_next_login: %>
                                You are required to change your password.
                        <%      break; %>
                        <% case LoginDetailStatus.Change_username_and_password_on_next_login: %>
                                You are required to change your username and password.
                        <%      break; %>
                    <% } %>
                </td>
            </tr>
            <% if (Model.ShowMessage) %>
            <% { %>
                <tr>
                    <td colspan="2" style="font-weight:bold;color:red;">
                        <div id="showmessage">
                            <% if (!string.IsNullOrEmpty(Model.Message)) %>
                            <% { %>
                                <%: Model.Message %>
                            <% } %>
                            <% else %>
                            <% { %>
                                An error occured updating your details, please contact the administrator.
                            <% } %>
                        </div>
                    </td>
                </tr>
            <% } %>
        
            <% switch (Model.LoginDetailStatus) %>
            <% { %>
                <% case LoginDetailStatus.Change_username_on_next_login: %>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                New Username
                            </td>
                            <td>
                                <%: Html.EditorFor(model => model.NewUserName) %>
                                <%: Html.ValidationMessageFor(model => model.NewUserName) %>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                Confirm New Username
                            </td>
                            <td>
                                <%: Html.EditorFor(model => model.RetypeNewUserName) %>
                                <%: Html.ValidationMessageFor(model => model.RetypeNewUserName) %>
                            </td>
                        </tr>
                <%      break; %>
                <% case LoginDetailStatus.Change_password_on_next_login: %>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                New Password
                            </td>
                            <td>
                                <%: Html.PasswordFor(model => model.NewPassword, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.NewPassword, string.Empty) }) %>
                                <%: Html.ValidationMessageFor(model => model.NewPassword) %>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                Confirm New Password
                            </td>
                            <td>
                                <%: Html.PasswordFor(model => model.RetypeNewPassword, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.RetypeNewPassword, string.Empty) }) %>
                                <%: Html.ValidationMessageFor(model => model.RetypeNewPassword) %>
                            </td>
                        </tr>
                <%      break; %>
                <% case LoginDetailStatus.Change_username_and_password_on_next_login: %>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                New Username
                            </td>
                            <td>
                                <%: Html.EditorFor(model => model.NewUserName) %>
                                <%: Html.ValidationMessageFor(model => model.NewUserName) %>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                Confirm New Username
                            </td>
                            <td>
                                <%: Html.EditorFor(model => model.RetypeNewUserName) %>
                                <%: Html.ValidationMessageFor(model => model.RetypeNewUserName) %>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                New Password
                            </td>
                            <td>
                                <%: Html.PasswordFor(model => model.NewPassword, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.NewPassword, string.Empty) }) %>
                                <%: Html.ValidationMessageFor(model => model.NewPassword) %>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;font-weight:bold;">
                                Confirm New Password
                            </td>
                            <td>
                                <%: Html.PasswordFor(model => model.RetypeNewPassword, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.RetypeNewPassword, string.Empty) }) %>
                                <%: Html.ValidationMessageFor(model => model.RetypeNewPassword) %>
                            </td>
                        </tr>
                <%      break; %>
            <% } %>
           <tr>
               <td colspan="2" style="text-align:center">
                   <input type="button" value="Save" id="save" />
               </td>
           </tr>
       <% } %>
       <tr>
           <td>
               <%: Html.HiddenFor(model => model.LoginDetailStatus) %>
           </td>
       </tr>
    </table>
<% } %>