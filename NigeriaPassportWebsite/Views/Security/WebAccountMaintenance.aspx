﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Passport.Models.Security.WebAccountMaintenanceViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Web account maintenance
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

        <%: Html.HiddenFor(model => model.WEB_ACCNT_ID) %>
        <%: Html.HiddenFor(model => model.ENTITY_ID) %>
                
        <table id="PageMainContent">
            <tr>
                <th colspan="3">
                    <div style="width:100%">
                        Web account maintenance
                    </div>
                </th>                    
            </tr>
            <tr>
                <td style="width:150px; font-weight:bold;">
                    <%: Html.LabelFor(model => model.LOGIN_NAME) %>
                </td>
                <td style="width:150px;">
                    <%: Html.DisplayFor(model => model.LOGIN_NAME) %>
                </td>
                <td>
                    <%: Html.ActionLink("Change Username", "AuthenticateChangeUserDetails", new { userDetailsChangeType = PassportCommon.Enumeration.Security.UserDetailsChangeType.UserName })%>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold;">
                    <%: Html.LabelFor(model => model.Password) %>
                </td>
                <td>
                    <%: Html.DisplayFor(model => model.Password, new { value = Model.Password }) %>
                </td>
                <td>
                    <%: Html.ActionLink("Change Password", "AuthenticateChangeUserDetails", new { userDetailsChangeType = PassportCommon.Enumeration.Security.UserDetailsChangeType.Password })%>
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold;">
                    <%: Html.LabelFor(model => model.EMAIL_ADDRESS) %>
                </td>
                <td>
                    <%: Html.DisplayFor(model => model.EMAIL_ADDRESS) %>                    
                    <%: Html.HiddenFor(model => model.EMAIL_ADDRESS) %>
                </td>
                <td>
                    <%: Html.ActionLink("Change Email Address", "AuthenticateChangeUserDetails", new { userDetailsChangeType = PassportCommon.Enumeration.Security.UserDetailsChangeType.Email })%>
                </td>
            </tr>
        </table>
<% } %>

</asp:Content>

