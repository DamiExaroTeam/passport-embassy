﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<PassportCommon.Models.Security.LoginModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">    
    Operator Login    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% if (ViewData["LoginError"] != null && Convert.ToBoolean(ViewData["LoginError"])) %>
    <% { %>
            <script  type="text/javascript">
                $(document).ready(function () {
                    $("#dialog-LoginError").dialog({
                        autoOpen: true,
                        resizable: false,
                        height: 200,
                        width: 400,
                        modal: true,
                        open: function (event, ui) {
                            $("button.ui-dialog-titlebar-close").hide();
                            $('.ui-dialog').css('z-index', 103);
                            $('.ui-widget-overlay').css('z-index', 102);
                            $("#dialog-LoginError").dialog("moveToTop");
                        },                       
                        buttons: {
                            "Ok": function () {
                                $(this).dialog("close");
                            }
                        }
                    });

                    $("#dialog-LoginError").dialog("moveToTop");
                });
	        </script>

            <div id="dialog-LoginError" title="An error occured">
	            <p><span style="float:left; margin:0 0 20px 0;"></span>An error occured authenticating your user name and password. Please make sure your user name and password is correct.</p>
            </div>        
    <% } %>

        <script type="text/javascript">

            $(document).ready(function () {

                $("#dialoghelp").dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: false,
                    height: 370,
                    width: 370,
                    show: {
                        effect: 'slide',
                        direction: 'right',
                        duration: 1000
                    },
                    hide: {
                        effect: 'drop',
                        direction: 'right',
                        duration: 1000
                    },
                    position: {
                        "my": "right top",
                        "at": "left top",
                        "of": $("#needhelpbanner")
                    },
                    closeOnEscape: true,
                    open: function (event, ui) {
                        $("button.ui-dialog-titlebar-close").hide();
                        $('.ui-dialog').css('z-index', 103);
                        $('.ui-widget-overlay').css('z-index', 102);
                        $("#dialoghelp").dialog("moveToTop");
                    },
                    buttons: {
                        Close: function () {
                            $(this).dialog("close");
                            
                        }
                    }
                });                
            })

            function openhelp() {
                $('#dialoghelp').attr('style', 'visibility: visible !important');

                $("#dialoghelp").dialog('open');
            }
    </script>

<% Html.EnableClientValidation(); %>

<div id="dialoghelp" title="Need Help?" style="visibility: hidden;" class="rightContent">
    <% Html.RenderPartial("PasswordRecovery"); %>
</div>

 <% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "LoginForm" }))
    { %>
    
    <div class="PageContentHeader">Login</div>

    <div id="needhelpdiv" class="ListHeader">
        <div id="needhelpbanner" onclick="openhelp()" >
            Need help?
        </div>
    </div> 
    
    <table id="InnerContent">                       
    <tr>
        <td colspan="2">
            If you have registered as a user, please enter your Username and Password below. Note that your password is case sensitive.                            
            To log in please supply the following information:<br /><br />
        </td>                         
    </tr>
    <tr>
        <td>
            Username or email address
        </td>
        <td>
            <%: Html.EditorFor(model => model.UserName)%><br />
            <%: Html.ValidationMessageFor(model => model.UserName)%>
        </td>      
    </tr>
    <tr>
        <td>
            Password
        </td>
        <td>
            <%: Html.PasswordFor(model => model.Password, new { value = Model == null ? "" : PassportCommon.Utilities.GeneralUtilities.IsNullObject(Model.Password, string.Empty) })%><br />
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </td>      
    </tr>
    <%-- <tr>
        <td>View Dashboard</td>
        <td> <%: Html.CheckBox("DashboardView") %>  </td>
    </tr>--%>
    <tr class="onelinetr">
        <td colspan="2">
        <br />
            Please follow the recovery procedure if you forgot your username or password
        </td>      
    </tr>
                    
    <tr>
        <td colspan="2" style="padding-left:10px;">                                
            <%: 
                @CaptchaMVC.HtmlHelpers.CaptchaHelper.Captcha(this.Html, 6)                            
            %>
            <font color="red"><%: Html.Label("Error", (TempData["error"] == null ? "" : TempData["error"].ToString()))%> </font>
        </td>                        
    </tr>
    <tr>                           
        <td colspan="2">                                
                <input type="submit" value="Log in" />                              
        </td>      
    </tr>
</table>
            
<% } %>

</asp:Content>

