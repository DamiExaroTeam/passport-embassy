﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.DateTime?>" %>                 

    <script src="<%: Url.Content("~/Scripts/ui//jquery.ui.datepicker.js") %>" type="text/javascript"></script>
   <script type="text/javascript">
    $(function() {
        $(".datePicker").datepicker({ changeMonth: true,changeYear: true});
    });

   </script>

   <%: Html.TextBox("", (Model.HasValue ? Model.Value.ToString("yyyy/MM/dd") : string.Empty), new { @class = "datePicker", style = "width:80px" })%>