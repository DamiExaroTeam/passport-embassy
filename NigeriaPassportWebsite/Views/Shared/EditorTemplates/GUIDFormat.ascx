﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PassportCommon.Models.Operator.GUIDFormat>" %>

<script src="<%: Url.Content("~/Scripts/TextboxKeypress.js") %>" type="text/javascript"></script>

<table>
    <tr>
        <td colspan="2">
            Please enter the operator validation code.
        </td>
    </tr>    
    <tr>
        <td style="width:15%; font-weight:bold;">
            <%: Html.LabelFor(model => model.ValidationGUID)%>
        </td>
        <td> 
            <%: Html.TextBoxFor(model => model.ValidationGUID1, new { style = "width:60px", id = "ValidationGUID1", @maxlength = 8, onkeypress = "return KeypressAlpaNumericOnly(event);" })%>
            &nbsp<span>-</span>&nbsp
            <%: Html.TextBoxFor(model => model.ValidationGUID2, new { style = "width:40px", id = "ValidationGUID2", @maxlength = 4, onkeypress = "return KeypressAlpaNumericOnly(event);" })%>
            &nbsp<span>-</span>&nbsp
            <%: Html.TextBoxFor(model => model.ValidationGUID3, new { style = "width:40px", id = "ValidationGUID3", @maxlength = 4, onkeypress = "return KeypressAlpaNumericOnly(event);" })%>
            &nbsp<span>-</span>&nbsp
            <%: Html.TextBoxFor(model => model.ValidationGUID4, new { style = "width:40px", id = "ValidationGUID4", @maxlength = 4, onkeypress = "return KeypressAlpaNumericOnly(event);" })%>
            &nbsp<span>-</span>&nbsp
            <%: Html.TextBoxFor(model => model.ValidationGUID5, new { style = "width:80px", id = "ValidationGUID5", @maxlength = 12, onkeypress = "return KeypressAlpaNumericOnly(event);" })%>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <%: Html.ValidationMessageFor(model => model.ValidationGUID1 )%>
            <%: Html.ValidationMessageFor(model => model.ValidationGUID2 )%>
            <%: Html.ValidationMessageFor(model => model.ValidationGUID3 )%>
            <%: Html.ValidationMessageFor(model => model.ValidationGUID4 )%>
            <%: Html.ValidationMessageFor(model => model.ValidationGUID5 )%>
        </td>
    </tr>    
</table>

