﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Passport.Models.Shared.MessageModel>" %>

<table id="PageMainContent">
    <tr>
            <th>
            <div>
                <%: Html.DisplayFor(model => model.MessageHeader) %>
            </div>                           
        </th>                 
    </tr>
    <tr>
        <td>
            <br />
                <%:Html.DisplayFor(model => model.Message) %>
            <br />
            <br />
            <% if(!string.IsNullOrEmpty(Model.ActionLinkText)) { %>                            
                <%: Html.ActionLink(Html.Encode(Model.ActionLinkText), Html.Encode(Model.ActionLinkAction), Html.Encode(Model.ActionLinkController), Model.ActionLinkRouteValues, Model.ActionLinkHTMLAttributes) %>
            <% } %>
        </td>                          
    </tr>                   
</table>
             
            

