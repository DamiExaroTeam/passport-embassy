﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Passport.Models.Shared.MessageModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Message
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<table id="PageMainContent">
    <tr>
            <th>
            <div>
                <%: Html.DisplayFor(model => model.MessageHeader) %>
            </div>                           
        </th>                 
    </tr>
    <tr>
        <td>
            <br />
                <%:Html.DisplayFor(model => model.Message) %>
            <br />
            <br />
            <% if(!string.IsNullOrEmpty(Model.ActionLinkText)) { %>                            
                <%: Html.ActionLink(Html.Encode(Model.ActionLinkText), Html.Encode(Model.ActionLinkAction), Html.Encode(Model.ActionLinkController), Model.ActionLinkRouteValues, Model.ActionLinkHTMLAttributes) %>
            <% } %>
        </td>                          
    </tr>                   
</table>

</asp:Content>

