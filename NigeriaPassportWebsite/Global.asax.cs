﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Passport.Models.Session;
using PassportProxy;
using Passport.Helpers;

namespace PassportIntraWebsite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*ajaxloader}", new { ajaxloader = @"(.*/)?ajax-loader.gif(/.*)?" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                new { controller = @"[^\.]*" }
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            ModelBinders.Binders.DefaultBinder = new Passport.Helpers.CustomModelBinder();

            //ModelBinders.Binders.Add(typeof(DateTime), new DateTimeBinder());
            //ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeBinder());
        }

        protected void Session_End()
        {
            SessionDataModel data = (SessionDataModel)Session["SessionData"];

            if (data.WebAccountId > 0)
            {
                //AuditTrailProxy proxy = new AuditTrailProxy();
                //proxy.channel.WebAccountActivitySessionEnd(data.WebAccountId, data.UserIP);
            }
        }
    }
}