﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Passport.Models.Security
{
    public class ChangeUsername
    {
        public long WebAccountId { get; set; }

        public string SecurityCode { get; set; }

        [Required(ErrorMessage = "Please provide a user name longer than 2 characters and shorter than 20.")]
        [StringLength(20, MinimumLength = 3)]
        public string NewUserName { get; set; }

        [Required(ErrorMessage = "Please provide a user name longer than 2 characters and shorter than 20.")]
        [StringLength(20, MinimumLength = 3)]
        [Compare("NewUserName", ErrorMessage = "The user name and confirmation user name do not match.")]
        public string NewUserNameRetype { get; set; }
    }
}