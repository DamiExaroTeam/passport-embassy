﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Passport.Models.Security
{
    public class ChangeUserDetails
    {
        [DisplayName("New Email Address")]
        [PassportCommon.Utilities.CustomModelAttributes.Email]
        [Required(ErrorMessage="Email address is required.")]
        public string NewEmail { get; set; }

        [DisplayName("Confirm New Email Address")]
        [Compare("NewEmail", ErrorMessage = "The email address and confirmation email addresses do not match.")]
        [PassportCommon.Utilities.CustomModelAttributes.Email]
        [Required(ErrorMessage = "Retyped email address is required.")]
        public string RetypeNewEmail { get; set; }
    }
}