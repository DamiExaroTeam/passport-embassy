﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Passport.Models.Security
{
    public class WebAccountMaintenanceViewModel : PassportCommon.Models.Security.WebAccountMaintenance
    {
        public const string UserMask = "***********";
        public const string PasswordMask = "***********";

        public WebAccountMaintenanceViewModel()
        {
            LOGIN_NAME = UserMask;
            Password = PasswordMask;
            RetypePassword = PasswordMask;
        }

        [Required(ErrorMessage = "Please provide a password longer than 5 characters and shorter than 20.")]
        [StringLength(20, MinimumLength = 6)]
        [DisplayName("Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string RetypePassword { get; set; }
    }
}