﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportCommon.Models.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Passport.Models.Security 
{
    public class SecurityQuestionsWeb : SecurityQuestions
    {
        public SelectList SecurityQuestions1 { get; set; }

        public SelectList SecurityQuestions2 { get; set; }
    }
}