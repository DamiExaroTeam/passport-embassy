﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using PassportCommon.Models.Security;

namespace Passport.Models.Security
{
    public class LoginDetailsChangeWeb : LoginDetailsChange
    {
        [Compare("NewUserName", ErrorMessage = "The username and confirmation username do not match.")]
        public string RetypeNewUserName { get; set; }

        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string RetypeNewPassword { get; set; }
    }
}