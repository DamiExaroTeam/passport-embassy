﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace NigeriaPassportWebsite.Models.Security
{
    public class LostLoginDetailsResetUser 
    {
        public LostLoginDetailsResetUser()
        {
            DisplaySecurityQuestionNumber = 1;
            SessionExpired = false;
            ShowIncorrectMessage = false;
        }

        public bool Verified { get; set; }

        public bool SecurityQuestionsExist { get; set; }

        public long EntityId { get; set; }

        public PassportCommon.Enumeration.WebAccount.WebAccountStatus WebAccountStatus { get; set; }

        public bool ReachedIncorrectAnswerLimit { get; set; }

        public string Email { get; set; }

        public int DisplaySecurityQuestionNumber { get; set; }

        public bool SessionExpired { get; set; }

        public bool ShowIncorrectMessage { get; set; }

        [DisplayName("Security Question 1")]
        public string SecurityQuestion1 { get; set; }

        [DisplayName("Security Question 2")]
        public string SecurityQuestion2 { get; set; }

        [DisplayName("Answer")]
        public string Answer { get; set; }
    }
}