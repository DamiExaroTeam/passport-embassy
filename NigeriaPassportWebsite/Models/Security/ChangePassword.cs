﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Passport.Models.Security
{
    public class ChangePassword
    {
        public long WebAccountId { get; set; }

        public string SecurityCode { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [PassportCommon.Utilities.CustomModelAttributes.Password]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [PassportCommon.Utilities.CustomModelAttributes.Password]
        [DisplayName("Retype Password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string NewPasswordRetype { get; set; }
    }
}