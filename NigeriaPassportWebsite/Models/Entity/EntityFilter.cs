﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace PassportIntraWebsite.Models.Entity
{
    public class EntityFilter
    {        
        [DisplayName("First Name")]        
        public string FIRSTNAME { get; set; }
                
        [DisplayName("Surname")]        
        public string SURNAME { get; set; }
        
        [DisplayName("Email Address")]
        public string EMAIL_ADDRESS { get; set; }
                
        [DisplayName("Mobile Number")]
        public string GSM_NUMBER { get; set; }
    }
}