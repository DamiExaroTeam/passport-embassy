﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NigeriaPassportWebsite.Models.Entity
{
    public class SubdivisionDelete
    {
        public int OperatorCount { get; set; }

        public string DivisionName { get; set; }

        public int DivisionID { get; set; }

        public int DivisionParentID { get; set; }
    }
}