﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Passport.Models.Shared
{
    public class MessageModel
    {
        public string MessageHeader { get; set; }

        public string Message { get; set; }

        public string ActionLinkText { get; set; }

        public string ActionLinkController { get; set; }

        public string ActionLinkAction { get; set; }

        public RouteValueDictionary ActionLinkRouteValues { get; set; }

        public Dictionary<string, object> ActionLinkHTMLAttributes { get; set; }
    }
}