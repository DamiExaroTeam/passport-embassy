﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Passport.Models.Operator
{
    public class OperatorRegistrationWeb : PassportCommon.Models.Operator.OperatorRegistration
    {        
        public OperatorRegistrationWeb()
        {
        }               

        #region User Information
        [Required(ErrorMessage = "Please retype your password exactly as the original.")]
        [DisplayName("Retype Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string RetypePassword { get; set; }
        #endregion

        #region Multiple Registration Fields(Additional)        
        [DisplayName("Email Address")]
        public string ADD_EMAIL_ADDRESS { get; set; }
                
        [StringLength(11)]
        [DisplayName("Mobile Number")]
        public string ADD_GSM_NUMBER { get; set; }

        #endregion
    }
}