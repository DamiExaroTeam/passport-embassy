﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using PassportProxy;
using PassportCommon.Models.Entity;

namespace Passport.Models.Session
{
    public class SessionData
    {
        //public static PassportCommon.Enumeration.Entity.EntityType CurrentEntityType 
        //{
        //    get
        //    {
        //        ////set the default entity type
        //        //if (GetSessionData.CurrentEntityType == 0)
        //        //{                    
        //        //    GetSessionData.CurrentEntityType = PassportCommon.Enumeration.Entity.EntityType.None;                    
        //        //}

        //        return GetSessionData.CurrentEntityType;
        //    }
        //    set
        //    {       GetSessionData.CurrentEntityType = value;
        //            RefreshRoles();
                
        //        if ( (PassportCommon.Enumeration.Entity.EntityType)value != GetSessionData.CurrentEntityType)
        //        {
        //            Logout();
                                        
        //            RefreshMenuItems(-1);
        //        }
        //    }
        //}

        public static long WebAccountId
        {
            get
            {
                return GetSessionData.WebAccountId;
            }
            set
            {
                GetSessionData.WebAccountId = value;

                if (value > 0)
                {
                    IsAuthenticated = true;
                }
                else
                {
                    IsAuthenticated = false;
                }

                RefreshMenuItems(value);
                RefreshAllowedUserActions(value);
                ResetEntityTypeSessionData();
            }
        }      

        //public static bool Dashboard
        //{
        //    get
        //    {
        //        return GetSessionData.Dashboard;
        //    }
        //    set
        //    {
        //        GetSessionData.Dashboard = value;
        //    }
        //}

        public static long EntityId
        {
            get
            {
                return GetSessionData.EntityId;
            }
            set
            {
                GetSessionData.EntityId = value;
            }
        }

        public static bool IsAuthenticated
        {
            get
            {
                return GetSessionData.IsAuthenticated;
            }
            set
            {
                GetSessionData.IsAuthenticated = value;
            }
        }

        public static string UserName
        {
            get
            {
                return GetSessionData.UserName;
            }
            set
            {
                GetSessionData.UserName = value;
            }
        }

        public static string FirstName
        {
            get
            {
                return GetSessionData.FirstName;
            }
            set
            {
                GetSessionData.FirstName = value;
            }
        }

        public static int DivisionID
        {
            get
            {
                return GetSessionData.DivisionID;
            }
            set
            {
                GetSessionData.DivisionID = value;
            }
        }

        public static string DivisionName
        {
            get
            {
                return GetSessionData.DivisionName;
            }
            set
            {
                GetSessionData.DivisionName = value;
            }
        }

        public static int DivisionLevel
        {
            get
            {
                return GetSessionData.DivisionLevel;
            }
            set
            {
                GetSessionData.DivisionLevel = value;
            }
        }

        public static bool IsAdministrator
        {
            get
            {
                return GetSessionData.IsAdministrator;
            }
            set
            {
                GetSessionData.IsAdministrator = value;
            }
        }

        public static bool PassportVerificationAllowed
        {
            get
            {
                return GetSessionData.PassportVerificationAllowed;
            }
            set
            {
                GetSessionData.PassportVerificationAllowed = value;
            }
        }

        public static PassportCommon.Enumeration.WebAccount.LoginDetailStatus CurrentLoginDetailStatus
        {
            get
            {
                return GetSessionData.CurrentLoginDetailStatus;
            }
            set
            {
                GetSessionData.CurrentLoginDetailStatus = value;
            }
        }

        public static List<PassportCommon.Models.Menu.Menu> CurrentUserMenu
        {
            get
            {
                if (GetSessionData.CurrentUserMenu == null)
                {
                    RefreshMenuItems(-1);
                }

                return GetSessionData.CurrentUserMenu;
            }
            set
            {
                GetSessionData.CurrentUserMenu = value;
            }
        }

        public static List<PassportCommon.Models.Security.InternalActionModel> AllowedUserActions
        {
            get
            {
                if (GetSessionData.AllowedUserActions == null)
                {
                    RefreshAllowedUserActions(-1);
                }

                return GetSessionData.AllowedUserActions;
            }
            set
            {
                GetSessionData.AllowedUserActions = value;
            }
        }

        public static List<string> AdditionalPrivilegeNames
        {
            get
            {
                if (GetSessionData.AdditionalPrivilegeNames == null)
                {
                    RefreshAllowedUserActions(-1);
                }

                return GetSessionData.AdditionalPrivilegeNames;
            }
            set
            {
                GetSessionData.AdditionalPrivilegeNames = value;
            }
        }

        //public static List<LevelDivision> LevelSubDivisions
        //{
        //    get
        //    {
        //        if (GetSessionData.LevelDivisions == null)
        //        {
        //            UpdateLevelDivisions();
        //        }

        //        return GetSessionData.LevelDivisions;
        //    }
        //    set
        //    {
        //        GetSessionData.LevelDivisions = value;
        //    }
        //}

        //public static List<OperatorEntity> OperatorEntities
        //{
        //    get
        //    {
        //        if (GetSessionData.OperatorList == null)
        //        {
        //            UpdateOperatorList();
        //        }

        //        return GetSessionData.OperatorList;
        //    }
        //    set
        //    {
        //        GetSessionData.OperatorList = value;
        //    }
        //}
        
        public static string UserIP
        {
            get
            {
                return GetSessionData.UserIP;
            }
            set
            {
                GetSessionData.UserIP = value;
            }
        }

        public static void Logout()
        {
            if (WebAccountId > 0)
            {
                AuditTrailProxy proxy = new AuditTrailProxy();
                //proxy.channel.WebAccountActivitySessionEnd(SessionData.WebAccountId, SessionData.UserIP);
            }

            LogoutSessionData();
            RefreshMenuItems(-1);
        }

        public static void RefreshRoles()
        {
            RefreshMenuItems(WebAccountId);
            RefreshAllowedUserActions(WebAccountId);
            ResetEntityTypeSessionData();
            RefreshExtraPriv(WebAccountId);
        }

        private static void RefreshExtraPriv(long webAccountId)
        {
            //SecurityProxy securityProxy = new SecurityProxy();

            //List<string> extraPrivNames = securityProxy.SecurityChannel.GetAdditionalPrivileges(webAccountId);

            //if (extraPrivNames == null)
            //    extraPrivNames = new List<string>();

            //GetSessionData.AdditionalPrivilegeNames = extraPrivNames;
        }

        private static void RefreshMenuItems(long NewWebAccountId)
        {
            SecurityProxy securityProxy = new SecurityProxy();            
            List<PassportCommon.Models.Menu.Menu> MenuItems = securityProxy.SecurityChannel.GetMenuItems(WebAccountId);

            GetSessionData.CurrentUserMenu = MenuItems;
        }

        private static void RefreshAllowedUserActions(long webAccountId)
        {
            SecurityProxy securityproxy = new SecurityProxy();

            List<PassportCommon.Models.Security.InternalActionModel> lstintact = securityproxy.SecurityChannel.GetAllowedInternalActions(webAccountId);

            if (lstintact == null || lstintact.Count <= 0)
                throw new ApplicationException("no internal actions returned.");

            GetSessionData.AllowedUserActions = lstintact;
        }

        ////Demo Data
        //public static void UpdateLevelDivisions()
        //{
        //    List<LevelDivision> levelEntities = new List<LevelDivision>();

        //    //Primary Division
        //    levelEntities.Add(new LevelDivision() { Name = "South Africa", SubDivisionID = 1, UserCount = 1 });

        //    //Primary Division Sub Divisions
        //    levelEntities.Add(new LevelDivision() { Name = "Home Affairs", SubDivisionID = 2, ParentSubDivisionID = 1, UserCount = 1 });
        //    levelEntities.Add(new LevelDivision() { Name = "Nigeria Embassy", SubDivisionID = 3, ParentSubDivisionID = 1, UserCount = 1 });
        //    levelEntities.Add(new LevelDivision() { Name = "Immigration(Border Control)", SubDivisionID = 4, ParentSubDivisionID = 1, UserCount = 1 });

        //    //Home Affairs Sub Divisions 
        //    levelEntities.Add(new LevelDivision() { Name = "Pretoria Headquarters", SubDivisionID = 5, ParentSubDivisionID = 2, UserCount = 2 });
        //    levelEntities.Add(new LevelDivision() { Name = "Centurion", SubDivisionID = 6, ParentSubDivisionID = 2, UserCount = 2 });

        //    //Nigeria Embassy Sub Divisions
        //    levelEntities.Add(new LevelDivision() { Name = "Abuja", SubDivisionID = 7, ParentSubDivisionID = 3, UserCount = 2 });
        //    levelEntities.Add(new LevelDivision() { Name = "Lagos", SubDivisionID = 8, ParentSubDivisionID = 3, UserCount = 2 });

        //    //Immigration(Border Control) Sub Divisions
        //    levelEntities.Add(new LevelDivision() { Name = "OR Tambo International", SubDivisionID = 9, ParentSubDivisionID = 4, UserCount = 2 });
        //    levelEntities.Add(new LevelDivision() { Name = "Cape Town International", SubDivisionID = 10, ParentSubDivisionID = 4, UserCount = 2 });
        //    levelEntities.Add(new LevelDivision() { Name = "King Shaka International", SubDivisionID = 11, ParentSubDivisionID = 4, UserCount = 2 });

        //    GetSessionData.LevelDivisions = levelEntities;
        //}


        //public static void UpdateOperatorList()
        //{
        //    List<OperatorEntity> operatorList = new List<OperatorEntity>();

        //    //South Africa
        //    operatorList.Add(new OperatorEntity() { EntityId = 1, Administrator = true, Firstname = "Herman", Surname = "Moleke", PassportVerification = false, DivisionID = 1,DOB = new DateTime(1977,11,21) });

        //    //Home Affairs
        //    operatorList.Add(new OperatorEntity() { EntityId = 2, Administrator = true, Firstname = "Kagiso", Surname = "Manye", PassportVerification = true, DivisionID = 2, DOB = new DateTime(1927, 10, 27) });

        //    //Nigeria Embassy
        //    operatorList.Add(new OperatorEntity() { EntityId = 3, Administrator = true, Firstname = "Anna", Surname = "Mahlangu", PassportVerification = false, DivisionID = 3, DOB = new DateTime(1954, 5, 5) });

        //    //Immigration(Border Control)
        //    operatorList.Add(new OperatorEntity() { EntityId = 4, Administrator = true, Firstname = "James", Surname = "Mitchell", PassportVerification = false, DivisionID = 4, DOB = new DateTime(1972, 3, 7) });

        //    //Pretoria Headquarters
        //    operatorList.Add(new OperatorEntity() { EntityId = 5, Administrator = true, Firstname = "Josephine", Surname = "Kolobe", PassportVerification = false, DivisionID = 5, DOB = new DateTime(1966, 5, 13) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 6, Administrator = false, Firstname = "Andre", Surname = "Koetzer", PassportVerification = true, DivisionID = 5, DOB = new DateTime(1922, 7, 18) });

        //    //Centurion
        //    operatorList.Add(new OperatorEntity() { EntityId = 7, Administrator = true, Firstname = "Mukele", Surname = "Tshabalala", PassportVerification = false, DivisionID = 6, DOB = new DateTime(1956, 1, 23) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 8, Administrator = false, Firstname = "Mbhongo", Surname = "Mphephu", PassportVerification = true, DivisionID = 6, DOB = new DateTime(1944, 11, 12) });

        //    //Abuja
        //    operatorList.Add(new OperatorEntity() { EntityId = 9, Administrator = true, Firstname = "Albert", Surname = "Mthumbu", PassportVerification = false, DivisionID = 7, DOB = new DateTime(1983, 2, 4) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 10, Administrator = true, Firstname = "Jameela", Surname = "Naidoo", PassportVerification = false, DivisionID = 7, DOB = new DateTime(1936, 8, 21) });

        //    //Lagos
        //    operatorList.Add(new OperatorEntity() { EntityId = 11, Administrator = true, Firstname = "Chahine", Surname = "Kaizawa", PassportVerification = false, DivisionID = 8, DOB = new DateTime(1957, 3, 21) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 12, Administrator = false, Firstname = "Akiloye", Surname = "Balewa ", PassportVerification = true, DivisionID = 8, DOB = new DateTime(1963, 9, 21) });

        //    //OR Tambo International
        //    operatorList.Add(new OperatorEntity() { EntityId = 13, Administrator = true, Firstname = "Falana", Surname = "Modise", PassportVerification = false, DivisionID = 9, DOB = new DateTime(1962, 12, 21) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 14, Administrator = true, Firstname = "Nnamani", Surname = "Ojukwu", PassportVerification = false, DivisionID = 9, DOB = new DateTime(1945, 4, 21) });

        //    //Cape Town International
        //    operatorList.Add(new OperatorEntity() { EntityId = 15, Administrator = true, Firstname = "Tinibu", Surname = "Omehia", PassportVerification = false, DivisionID = 10, DOB = new DateTime(1936, 5, 21) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 16, Administrator = false, Firstname = "Matthew", Surname = "Edwards", PassportVerification = true, DivisionID = 10, DOB = new DateTime(1962, 7, 21) });

        //    //King Shaka International
        //    operatorList.Add(new OperatorEntity() { EntityId = 17, Administrator = true, Firstname = "Malik", Surname = "Sharma", PassportVerification = false, DivisionID = 11, DOB = new DateTime(1966, 2, 2) });
        //    operatorList.Add(new OperatorEntity() { EntityId = 18, Administrator = false, Firstname = "Mainza", Surname = "Abeni", PassportVerification = true, DivisionID = 11, DOB = new DateTime(1983, 3, 2) });

        //    GetSessionData.OperatorList = operatorList;
        //}
        
        private static SessionDataModel GetSessionData
        {
            get
            {
                SessionDataModel SessionDataMod;

                if (HttpContext.Current.Session["SessionData"] == null)
                {
                    SessionDataMod = new SessionDataModel();
                    HttpContext.Current.Session["SessionData"] = SessionDataMod;
                }
                else
                {
                    SessionDataMod = (SessionDataModel)HttpContext.Current.Session["SessionData"];
                }
                return SessionDataMod;
            }
        }

        private static void ResetEntityTypeSessionData()
        {
            HttpContext.Current.Session["EntityTypeSessionData"] = null;
        }

        private static EntityTypeSessionData GetEntityTypeSessionData
        {
            get
            {
                EntityTypeSessionData EntityTypeSessionDataMod;

                if (HttpContext.Current.Session["EntityTypeSessionData"] == null)
                {
                    EntityTypeSessionDataMod = new EntityTypeSessionData(WebAccountId, EntityId);
                    HttpContext.Current.Session["EntityTypeSessionData"] = EntityTypeSessionDataMod;
                }
                else
                {
                    EntityTypeSessionDataMod = (EntityTypeSessionData)HttpContext.Current.Session["EntityTypeSessionData"];
                }
                return EntityTypeSessionDataMod;
            }
        }

        private static void LogoutSessionData()
        {
            if (GetSessionData.SessionCache != null)
            {
                GetSessionData.SessionCache.ClearSessionCache();
            }
            HttpContext.Current.Session["SessionData"] = null;
            HttpContext.Current.Session["EntityTypeSessionData"] = null;
        }

        #region Session Cache

        public static Cache.SessionCache SessionCache
        {
            get
            {
                if (GetSessionData.SessionCache == null)
                {
                    GetSessionData.SessionCache = new Cache.SessionCache();
                }
                return GetSessionData.SessionCache;
            }
        }

        #endregion

        //#region Entity type specific data
        //public static bool IsIstlAdmin
        //{
        //    get
        //    {
        //        if (GetEntityTypeSessionData.EntityTypes.Contains (PassportCommon.Enumeration.Entity.EntityType.IstlAdmin))
        //        {
        //            return true;
        //        }
        //        return false;
        //    }
        //} 
      
       // #endregion
    }
}