﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PassportCommon.Models.Entity;

namespace Passport.Models.Session
{
    public class SessionDataModel
    {
        public SessionDataModel()
        {
            UserName = "";
            FirstName = "";        
            UserIP = "";
        }

        public long WebAccountId { get; set; }
                
        public long EntityId { get; set; }

        public bool Dashboard { get; set; }

        public bool IsAuthenticated { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public PassportCommon.Enumeration.WebAccount.LoginDetailStatus CurrentLoginDetailStatus { get; set; }

        public List<PassportCommon.Models.Menu.Menu> CurrentUserMenu { get; set; }

        public List<PassportCommon.Models.Security.InternalActionModel> AllowedUserActions { get; set; }

        public List<string> AdditionalPrivilegeNames { get; set; }

        public string UserIP { get; set; }

        public int DivisionID { get; set; }

        public int DivisionLevel { get; set; }

        public string DivisionName { get; set; }

        public bool IsAdministrator { get; set; }

        public bool PassportVerificationAllowed { get; set; }

        public List<LevelDivision> LevelDivisions { get; set; }

        public List<OperatorEntity> OperatorList { get; set; }

        public Cache.SessionCache SessionCache;
    }
}