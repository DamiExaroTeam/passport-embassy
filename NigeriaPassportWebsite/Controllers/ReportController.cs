﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Passport.Helpers;
using System.Globalization;
using PassportProxy;
using Passport.Models.Session;
using ReportTemplates;

namespace NigeriaPassportWebsite.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        [RestrictAccess]
        public ActionResult PassportVerificatonDetail()
        {
            List<KeyValuePair<string, string>> sortList = new List<KeyValuePair<string, string>>();
            sortList.Add(new KeyValuePair<string, string>("PASSPORT_HIST.CREATE_DATE", "Date"));
            sortList.Add(new KeyValuePair<string, string>("INPUT_DIVISION_NAME", "Division Name"));
            sortList.Add(new KeyValuePair<string, string>("INPUT_SURNAME", "Operator Surname"));
            sortList.Add(new KeyValuePair<string, string>("PASSPORT_HIST.ADD_NOTE", "Note"));

            ViewData["SortListItems"] = new SelectList(sortList, "Key", "Value");

            EntityProxy px = new EntityProxy();
            var divList = px.EntityChannel.GetDivisionTree(SessionData.DivisionID);

            ViewData["DivisionSelect"] = new SelectList(divList, "DivisionID", "FullName");

            return View();
        }

        [HttpPost]
        public ActionResult PassportVerificatonDetail(FormCollection formData)
        {            
            string filterString = string.Empty;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();
            int divisionID = 0;
            int convInt = 0;

            DateTime.TryParseExact(formData["fromDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out fromDate);
            DateTime.TryParseExact(formData["toDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out toDate);

            filterString = "Verifications from {0} to {1}";
            filterString = string.Format(filterString, fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));

            if (int.TryParse(formData["DivisionList"], out convInt))
            {
                if (convInt != 0)
                    divisionID = convInt;
            }

            ReportProxy proxy = new ReportProxy();
            string sortList = formData["sortList"];

            var data = proxy.ReportChannel.GetPassportVerificationData(fromDate, toDate, sortList, divisionID);

            ViewData["Data"] = data;
            var report = new PassportDetailHistReport();
            report.PageSettings.Landscape = true;
            report.DataSource = data;

            report.ReportParameters["FilterString"].Value = filterString;

            ViewData["Report"] = report;
         
            return View("TelerikNoMaster");
        }

        [RestrictAccess]
        public ActionResult PassportVerificatonSummary()
        {           
            return View();
        }

        [HttpPost]
        public ActionResult PassportVerificatonSummary(FormCollection formData)
        {
            string filterString = string.Empty;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();

            DateTime.TryParseExact(formData["fromDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out fromDate);
            DateTime.TryParseExact(formData["toDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out toDate);

            filterString = "Verifications from {0} to {1}";
            filterString = string.Format(filterString, fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));

            ReportProxy proxy = new ReportProxy();
            string sortList = formData["sortList"];

            var data = proxy.ReportChannel.GetPassportVerificationDataSummary(fromDate, toDate, SessionData.DivisionID);

            ViewData["Data"] = data;
            var report = new PassportSummaryReport();
            report.PageSettings.Landscape = true;
            report.DataSource = data;

            report.ReportParameters["FilterString"].Value = filterString;

            ViewData["Report"] = report;

            return View("TelerikNoMaster");
        }

        [RestrictAccess]
        public ActionResult PassportValidation(string docNo, string surname, string firstName, string gender, DateTime birthDate, string stageDescription, long historyID, string otherNames, DateTime issueDate,DateTime expiryDate,string docType,string issuePlace)
        {
            PassportCommon.Models.Passport.Passport passport = new PassportCommon.Models.Passport.Passport();
            passport.DocNo = docNo;
            passport.Surname = surname;
            passport.FirstName = firstName;
            passport.Gender = gender;
            passport.BirthDate = birthDate;
            passport.StageDescription = stageDescription;
            passport.HistoryID = historyID;
            passport.IssueDate = issueDate;
            passport.OtherNames = otherNames;
            passport.ExpiryDate = expiryDate;
            passport.DocType = docType;
            passport.IssuePlace = issuePlace;

            ImageController ic = new ImageController();

            passport.TempFaceImage = ic.GetPassportImage(docNo, true);
            passport.TempSignImage = ic.GetPassportImage(docNo, false);

            ViewData["Data"] = passport;
            var report = new PassportValidation();
            //report.PageSettings.Landscape = true;
            report.DataSource = passport;

            ViewData["Report"] = report;

            return View("TelerikNoMaster");
            //return new ReportPdfResult(report);            
        }
    }
}
