﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Passport.Models.Shared;
using PassportProxy;
using PassportCommon.Models.Passport;
using Passport.Helpers;
using Passport.Models.Session;

namespace NigeriaPassportWebsite.Controllers
{
    public class PassportVerificationController : BaseController
    {        
        [RestrictAccess]
        public ActionResult VerifyPassport()
        {
            if (!SessionData.PassportVerificationAllowed)
            {
                MessageModel model = new MessageModel();
                model.MessageHeader = "Authentication required";
                model.Message = "You don't have access to this part of the application.";
                model.ActionLinkAction = "Index";
                model.ActionLinkController = "Home";
                model.ActionLinkText = "Home";

                return View("ShowMessage", model);
            }

            return View(new PassportCommon.Models.Passport.Passport());
        }

        public ActionResult ShowPassport(string identificationGuid, string passportNumber)
        {
            VerificationProxy vp = new VerificationProxy();

            //Get All from service cache
            List<PassportCommon.Models.Passport.Passport> passportsVerified = vp.VerificationChannel.RetreivePassport(identificationGuid);

            PassportCommon.Models.Passport.Passport passportVerified = passportsVerified.SingleOrDefault(c => c.DocNo == passportNumber);

            if (passportVerified.IssueDate.AddYears(5) < DateTime.Now)
            {
                if (passportVerified.StageCode == "EM5000")
                {
                    passportVerified.StageCode = "0";
                    passportVerified.StageDescription = "EXPIRED";
                }
                else
                {
                    passportVerified.StageCode = "0";
                    passportVerified.StageDescription += " / EXPIRED";
                }
            }

            MultiPassport mp = new MultiPassport();
            mp.Passport = passportVerified;
            mp.IdentificationGuid = passportVerified.IdentificationGuid;

            mp.PassportHistory = (from c in passportsVerified
                                  select new PassportMin() { PassportNumber = c.DocNo, PassportDate = c.IssueDate }).ToList();
                        
            string html = ViewToString.RenderViewToString(this, "ShowMultiPassport", mp);

            return Json(html, JsonRequestBehavior.AllowGet);
            //return PartialView("ShowMultiPassport", mp);
        }

        [HttpPost]
        public ActionResult VerifyPassport(PassportCommon.Models.Passport.Passport passportSelection, FormCollection fc)
        {
            if (Request.IsAjaxRequest())
            {
                VerificationProxy vp = new VerificationProxy();

                if (passportSelection.DocNo != null && passportSelection.DocNo.Length > 0 && !string.IsNullOrWhiteSpace(passportSelection.Surname))
                {
                    if (!ValidPassportNo(passportSelection.DocNo))
                    {
                        MessageModel mModel = new MessageModel();
                        mModel.Message = "Please check the passport number. It needs to be 9 characters long, start with an A,D or F followed by 8 numbers.";
                        return PartialView("MessageView", mModel);
                    }

                    string message = string.Empty;
                    bool multiFail = false;

                    passportSelection.DocNo = passportSelection.DocNo.ToUpper();

                    //For test use static data
                    //PassportCheckData oprData = new PassportCheckData() { OperatorEntityID = 1 };//SessionData.EntityId};


                    long operatorEntityID = SessionData.EntityId;
                    int divisionID = SessionData.DivisionID;

                    //Taken out for demo
                    if (SessionData.EntityId == 0)
                    {
                        MessageModel mModel = new MessageModel();
                        mModel.Message = "Your session ended. Please log in again.";
                        return PartialView("MessageView", mModel);
                    }

                    bool allTypes = fc["AllTypes"] == "false" ? false : true;

                    if(allTypes)
                        System.Threading.Thread.Sleep(60000);

                    //Search Passport
                    List<PassportCommon.Models.Passport.Passport> passportsVerified = vp.VerificationChannel.VerifyPassport(passportSelection.DocNo, passportSelection.Surname, operatorEntityID, divisionID, out message, out multiFail,allTypes);

                    if (passportsVerified == null)
                    {
                        if (multiFail)
                        {
                            return JavaScript("MultiFail();");
                        }

                        MessageModel mModel = new MessageModel();
                        mModel.Message = message;
                        return PartialView("MessageView", mModel);
                    }

                    PassportCommon.Models.Passport.Passport passportVerified = passportsVerified.SingleOrDefault(c => c.DocNo == passportSelection.DocNo);

                    if (passportVerified == null || message.Length > 0)
                    {
                        if (multiFail)
                        {
                            return JavaScript("MultiFail();");
                        }

                        MessageModel mModel = new MessageModel();
                        mModel.Message = message;
                        return PartialView("MessageView", mModel);
                    }

                    if (passportVerified.IssueDate.AddYears(5) < DateTime.Now)
                    {
                        if (passportVerified.StageCode == "EM5000")
                        {
                            passportVerified.StageCode = "0";
                            passportVerified.StageDescription = "EXPIRED";
                        }
                        else
                        {
                            passportVerified.StageCode = "0";
                            passportVerified.StageDescription += " / EXPIRED";
                        }
                    }

                    MultiPassport mp = new MultiPassport();
                    mp.Passport = passportVerified;
                    mp.IdentificationGuid = passportVerified.IdentificationGuid;

                    mp.PassportHistory = (from c in passportsVerified
                                          select new PassportMin() { PassportNumber = c.DocNo, PassportDate = c.IssueDate }).ToList();

                    mp.FullPassportHistoryList = new List<PassportCommon.Models.Passport.Passport>();
                    mp.FullPassportHistoryList.Add(passportVerified);
                    passportsVerified.Remove(passportVerified);
                    mp.FullPassportHistoryList.AddRange(passportsVerified.OrderByDescending(c => c.IssueDate));

                    return PartialView("ShowPassportCompare", mp);
                }
                else
                {
                    MessageModel mModel = new MessageModel();
                    mModel.Message = "Please enter a Passport Number and surname to verify";
                    return PartialView("MessageView", mModel);
                }
            }

            return View(passportSelection);
        }

        private bool ValidPassportNo(string passportNo)
        {
            if (passportNo.Trim().Length != 9)
                return false;

            char[] options = ("ADF").ToCharArray();

            if (!options.Contains(passportNo.ToUpper()[0]))
                return false;

            foreach (Char chr in passportNo.Substring(1, 8))
            {
                if (!Char.IsNumber(chr))
                    return false;
            }

            return true;
        }

        public ActionResult MultiFailLogout()
        {
            string messageHeader = "Account Suspended";
            string message = "After multiple failed passport verifications, your account has been suspended. Please contact an Administrator.";

            var values = new System.Web.Routing.RouteValueDictionary();
            values.Add("messageHeader", messageHeader);
            values.Add("message", message);

            return RedirectToAction("LogoutFailure", "Home", values);
        }
    }
}
