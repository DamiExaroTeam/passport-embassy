﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportProxy;

namespace NigeriaPassportWebsite.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult NotFound(string url)
        {
            var originalUri = url ?? Request.QueryString["aspxerrorpath"] ?? Request.Url.OriginalString; 

            try
            {
                SecurityProxy sp = new SecurityProxy();
                sp.SecurityChannel.LogError(base.MapException(new ApplicationException("Error 404 :"+originalUri)));
            }
            catch { }            
            
            return View();
        }
        
        public ActionResult SystemError()
        {
            if (Request.IsAjaxRequest())
            {
                return RedirectToAction("SystemErrorPartial");
            }

            return View();
        }

        public ActionResult SystemErrorPartial()
        {
            return View();
        }
    }
}
