﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportCommon.Models.Menu;
using Passport.Models.Shared;
using PassportCommon.Models.Menu;
using Passport.Models.Session;

namespace NigeriaPassportWebsite.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index(bool loggedOut = false, int entityType = 0)
        {
            if (loggedOut)
            {
                DoLogout();
            }
            
            return View();
        }

        public ActionResult LogoutFailure(string messageHeader,string message)
        {
            DoLogout();

            MessageModel model = new MessageModel();
            model.Message = message;
            model.MessageHeader = messageHeader; 

            return View("ShowMessage", model);
        }

        public void DoLogout()
        {
            Passport.Models.Session.SessionData.Logout();
            Session.Clear();
            Session.Abandon();
        }

        [ChildActionOnly]
        public ActionResult Menu()
        {
            List<PassportCommon.Models.Menu.Menu> MenuItems = Passport.Models.Session.SessionData.CurrentUserMenu;

            List<PassportCommon.Models.Menu.Menu> Menu = new List<PassportCommon.Models.Menu.Menu>();

            if (MenuItems != null && MenuItems.Count > 0)
            {
                List<Menu> parentItems = new List<Menu>();

                //if (Passport.Models.Session.SessionData.CurrentEntityType != PassportCommon.Enumeration.Entity.EntityType.None)
                //{
                //    parentItems = (from c in MenuItems
                //                   where c.PARENT_ID == -1 && c.ENTITY_TYPE_ID == (int)Passport.Models.Session.SessionData.CurrentEntityType
                //                   orderby c.SEQUENCE
                //                   select c).ToList();
                //}
                //else
                //{
                    parentItems = (from c in MenuItems
                                   where c.PARENT_ID == -1
                                   orderby c.SEQUENCE
                                   select c).ToList();
                //}
                
                foreach  (PassportCommon.Models.Menu.Menu m in parentItems)
                {                    
                    var childItems = (from c in MenuItems
                                      where c.PARENT_ID == m.MENU_ID
                                      orderby c.SEQUENCE
                                      select c).ToList();

                    m.ChildItems.Clear();
                    
                    //throw
                    //if (!SessionData.IsAdministrator)
                    //{
                    //    if (m.NAME == "Reports")
                    //        continue;

                    //    if (childItems.Count(c => c.NAME == "Division Management") > 0)
                    //        childItems.Remove(childItems.Single(c => c.NAME == "Division Management"));

                    //    //if (childItems.Count(c => c.NAME == "Verificaton Summary") > 0)
                    //    //    childItems.Remove(childItems.Single(c => c.NAME == "Verificaton Summary"));

                    //    //if (childItems.Count(c => c.NAME == "Verification Detail") > 0)
                    //    //    childItems.Remove(childItems.Single(c => c.NAME == "Verification Detail"));
                    //}

                    //if (!SessionData.PassportVerificationAllowed && childItems.Count(c => c.NAME == "Passport Verification") > 0)
                    //    childItems.Remove(childItems.Single(c => c.NAME == "Passport Verification"));

                    m.ChildItems.AddRange(childItems);

                    Menu.Add(m);
                }

                return PartialView(Menu);
            }
            else
            {
                return null;
            }
        }

        [ChildActionOnly]
        public ActionResult LoggedInMessage()
        {
            string message = "You are not logged in";
            ViewData["MessageExtended"] = string.Empty;
            ViewData["ControllerName"] = "Security";
            if (Passport.Models.Session.SessionData.IsAuthenticated)
            {
                message = "Welcome " + Passport.Models.Session.SessionData.FirstName;
                ViewData["ActionTypeText"] = "Log Out";
                ViewData["ActionName"] = "Logout";                
            }
            else
            {
                ViewData["ActionTypeText"] = "Log In";
                ViewData["ActionName"] = "Login";
            }
            ViewData["Message"] = message;
            return PartialView();
        }

       
    }
}
