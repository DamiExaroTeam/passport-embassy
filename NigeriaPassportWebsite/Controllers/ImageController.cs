﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportProxy;
using PassportCommon.Utilities;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace NigeriaPassportWebsite.Controllers
{
    public class ImageController : BaseController
    {
        public Image GetPassportImage(string passportNo, bool isFaceImage)
        {
            EntityProxy entProxy = new EntityProxy();
            byte[] passportImage = null;

            passportImage = entProxy.EntityChannel.GetPassportImage(passportNo, isFaceImage);

            return ImageUtil.ByteArrayToImage(passportImage);
        }        
   
        public ActionResult PassportImage(string passportNo, bool isFaceImage)
        {
            EntityProxy entProxy = new EntityProxy();
            byte[] passportImage = null;

            passportImage = entProxy.EntityChannel.GetPassportImage(passportNo, isFaceImage);

            if (passportImage != null)
            {
                return File(passportImage, "image/jpg");
            }
            else
            {
                return File(HttpContext.Server.MapPath("~/Content/Images/EmptySignatureImage.jpg"), "image/jpg");
            }
        }
    }
}
