﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportProxy;
using PassportCommon.Enumeration.WebAccount;
using NigeriaPassportWebsite.Controllers;

namespace NigeriaPassportWebsite.Controllers
{
    public class TestController : BaseController
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            if (PassportCommon.Utilities.GeneralUtilities.IsDeveloper ||
                PassportCommon.Utilities.GeneralUtilities.IsTestEnvironment)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        
        
        [HttpPost]
        public ActionResult Index(long webAccountID, int validationType)
        {
            if (PassportCommon.Utilities.GeneralUtilities.IsDeveloper ||
                PassportCommon.Utilities.GeneralUtilities.IsTestEnvironment)
            {
                MessageProxy proxy = new MessageProxy();
                string code;
                if (proxy.channel.GetActiveVerificationCode(webAccountID, (PassportCommon.Enumeration.WebAccount.VerificationType)validationType, out code))
                {
                    TempData["Result"] = code;
                }
            
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        public ActionResult ViewVariables()
        {
            if (PassportCommon.Utilities.GeneralUtilities.IsDeveloper ||
                PassportCommon.Utilities.GeneralUtilities.IsTestEnvironment)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
