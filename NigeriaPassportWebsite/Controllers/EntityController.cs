﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassportCommon.Models.Entity;
using Passport.Models.Session;
using System.Globalization;
using PassportProxy;
using NigeriaPassportWebsite.Models.Entity;
using PassportCommon.Enumeration.WebAccount;
using Passport.Models.Shared;
using Passport.Helpers;

namespace NigeriaPassportWebsite.Controllers
{
    public class EntityController : BaseController
    {
        [RestrictAccess]
        public ActionResult EntityHome()
        {            
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();
                      
            return ShowSubdivision(SessionData.DivisionID);
        }

        private ActionResult ReturnNotAllowed()
        {
            MessageModel model = new MessageModel();
            model.MessageHeader = "Authentication required";
            model.Message = "You don't have access to this part of the application.";
            model.ActionLinkAction = "Index";
            model.ActionLinkController = "Home";
            model.ActionLinkText = "Home";

            return View("ShowMessage", model);
        }
                
        public ActionResult ShowSubdivision(int id)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            int depth = 0;

            ViewData["AllowChange"] = true;

            ViewData["AllowChangeDivision"] = true;
            
            List<LevelDivision> menuNames = new List<LevelDivision>();
            //Get Division Details by ID            
            BuildDivisionName(id,SessionData.DivisionLevel, out depth,out menuNames);
            ViewData["Breadcrumb"] = menuNames;

            ViewData["ParentSubDivisionID"] = id;
                        
            if (depth > 2)
            {
                //Disable Changes - Only allowed Own level and one below
                ViewData["AllowChange"] = false;
            }

            List<OperatorEntity> personsList = null;

            if (depth < 1)
            {                
                //Not allowed to see upper levels
                int userParentDiv = ep.EntityChannel.GetDivision(SessionData.DivisionID).ParentDivisionID; 

                personsList = ep.EntityChannel.GetOperators(userParentDiv);

                var admins = personsList.Where(c => c.Administrator == true && c.WebAccountStatus == WebAccountStatus.Active);

                return View("AdministratorContactList", admins);
            }
            
            if(depth > 2)//Max level
                ViewData["AllowChangeDivision"] = false;

            if(menuNames.Count > 2)
                ViewData["HideDivision"] = true; 
            else
                ViewData["HideDivision"] = false;            
           
            personsList = ep.EntityChannel.GetOperators(id);

            ViewData["Administrators"] = personsList.Where(c => c.DivisionID == id && c.Administrator == true && c.WebAccountStatus != WebAccountStatus.Deleted).OrderBy(c => c.WebAccountStatus);

            ViewData["SystemUsers"] = personsList.Where(c => c.DivisionID == id && c.Administrator == false && c.PassportVerification == true && c.WebAccountStatus != WebAccountStatus.Deleted).OrderBy(c => c.WebAccountStatus);
            
            ViewData["LevelSubDivision"] = ep.EntityChannel.GetDivisionsFromParent(id);

            return View("EntityHome");
        }
                
        public ActionResult SuspendEntity(long id, int divisionID)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();
            ep.EntityChannel.SetEntityStatus(id,WebAccountStatus.Suspended);            
            
            return ShowSubdivision(divisionID);
        }
                
        public ActionResult ActivateEntity(long id, int divisionID)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();
            ep.EntityChannel.SetEntityStatus(id, WebAccountStatus.Active);            
            
            return ShowSubdivision(divisionID);
        }
                
        public ActionResult DeleteEntity(long id, int divisionID)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();
            ep.EntityChannel.SetEntityStatus(id, WebAccountStatus.Deleted);            

            return ShowSubdivision(divisionID);
        }
                
        public ActionResult EditEntity(long id)//Operator
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            var model = ep.EntityChannel.GetOperator(id);

            LoadEntityDropdowns();

            return View(model);
        }
                
        [HttpPost]
        public ActionResult EditEntity(OperatorEntity model)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            if (!ModelState.IsValid)
            {
                LoadEntityDropdowns();
                return View(model);
            }

            EntityProxy ep = new EntityProxy();
            ep.EntityChannel.UpdateEntity(model);

            return ShowSubdivision(model.DivisionID);
        }
                
        public ActionResult AddEntity(int divisionID, bool isAdmin, bool? succesfulsave)//Operator
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            OperatorEntity model = new OperatorEntity();
            model.DivisionID = divisionID;// SessionData.DivisionID;
            model.DOB = DateTime.Now;            
            model.EntityId = 0;
            model.Administrator = isAdmin;
            if (!isAdmin)
                model.PassportVerification = true;

            LoadEntityDropdowns();

            if (succesfulsave != null &&
                Convert.ToBoolean(succesfulsave))
            {
                ViewData["SuccessfulSave"] = true;
            }

            return View("EditEntity",model);
        }
                
        [HttpPost]
        public ActionResult AddEntity(OperatorEntity model)
        {
            if (!model.Administrator)
                model.PassportVerification = true;

            LoadEntityDropdowns();
            if (!ModelState.IsValid)
            {
                return View("EditEntity", model);
            }

            model.WebAccountStatus = WebAccountStatus.New;

            EntityProxy ep = new EntityProxy();
            if (ep.EntityChannel.AddUser(model, SessionData.EntityId))
            {
                ModelState.Clear();
                return RedirectToAction("AddEntity", new { divisionID = model.DivisionID, isAdmin = model.Administrator, succesfulsave = true });
            }
            else
            {
                return RedirectToAction("SystemError", "Error");
            }
        }

        public JsonResult VerifyEmailAddress(string emailaddress, int divisionId, long entityId)
        {
            EntityProxy proxy = new EntityProxy();
            Dictionary<string, object> result = new Dictionary<string, object>();
            string htmlview;

            EntityEmailReplace model = new EntityEmailReplace(emailaddress, divisionId, entityId);

            if (!proxy.EntityChannel.CheckEmailInUse(ref model))
            {
                result.Add("erroronsave", true);
            }
            else
            {
                if (model.InUse)
                {
                    if (model.CanReplace)
                    {
                        model.Message = "The specified email address is already in use in division: " + model.DivisionNameUsedFor + ". Do you wish to continue and replace the existing user?";
                    }
                    else
                    {
                        model.Message = "The specified email address is already in use in division: " + model.DivisionNameUsedFor + ". You need to specify a different email address.";
                    }

                    result.Add("inuse", true);

                    htmlview = PassportIntraWebsite.Utilities.GeneralWebUtilities.RenderPartialViewToHTMLString(this, "VerifyEmailAddress", model);
                    result.Add("messageview", htmlview);
                }
                else
                {
                    result.Add("inuse", false);
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
                
        public ActionResult ViewEntity(long id)//Operator
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            OperatorEntity model = ep.EntityChannel.GetOperator(id);

            int dummy = 0;
            List<LevelDivision> dummyList = new List<LevelDivision>();

            model.DivisionName = BuildDivisionName(model.DivisionID,1,out dummy,out dummyList);
            
            return View("ViewEntity",model);
        }

        public ActionResult RemovePassportVerification(long entityID)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            ep.EntityChannel.AllowPassportVerification(entityID, false);

            return ViewEntity(entityID);
        }

        public ActionResult AddPassportVerification(long entityID)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            ep.EntityChannel.AllowPassportVerification(entityID, true);

            return ViewEntity(entityID);
        }
       
        public ActionResult EditSubdivision(int id)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            LevelDivision division = ep.EntityChannel.GetDivision(id);
                        
            FillDivisionClassDropdownData(division.DivisionClassID);

            return View(division);
        }

        private void FillDivisionClassDropdownData(int selectedID)
        {
            EntityProxy ep = new EntityProxy();

            var divList = ep.EntityChannel.GetDivisionClassList();
            var selectList = new SelectList(divList, "ClassID", "ClassName",selectedID.ToString());

            ViewData["DivisionClassText"] = divList.Single(c => c.ClassID == selectedID);
            ViewData["DivisionClass"] = selectList;
        }

        [HttpPost]
        public ActionResult EditSubdivision(LevelDivision model,FormCollection fc)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();                        
                        
            EntityProxy ep = new EntityProxy();

            if (ep.EntityChannel.DivisionExists(model.ParentDivisionID, model.Name))
            {
                ModelState.AddModelError("Name", "a Division with this name already exists");
                FillDivisionClassDropdownData(model.DivisionClassID);
                return View("EditSubdivision", model);
            }

            ep.EntityChannel.UpdateDivision(model);

            return ShowSubdivision(model.ParentDivisionID);
        }
                
        public ActionResult DeleteSubdivision(int id)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            var operators = ep.EntityChannel.GetOperators(id);
            LevelDivision delModel = ep.EntityChannel.GetDivision(id);
            
            SubdivisionDelete sDel = new SubdivisionDelete();

            sDel.DivisionID = id;
            sDel.DivisionParentID = delModel.ParentDivisionID;
            sDel.DivisionName = delModel.Name;
            sDel.OperatorCount = operators.Count();

            return View("ConfirmSubdivisionDelete", sDel);
        }

        [HttpPost]
        public ActionResult DeleteSubdivision(SubdivisionDelete subDiv)
        {
            return DeleteDivision(subDiv.DivisionID, subDiv.DivisionParentID);
        }
                
        public ActionResult DeleteDivision(int divisionID,int divisionParentID)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            EntityProxy ep = new EntityProxy();

            ep.EntityChannel.DeleteDivision(divisionID);

            return ShowSubdivision(divisionParentID);
        }
                
        public ActionResult AddSubdivision(int id)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();

            FillDivisionClassDropdownData(1);

            LevelDivision division = new LevelDivision() { ParentDivisionID = id };

            return View("EditSubdivision", division);
        }
                
        [HttpPost]
        public ActionResult AddSubdivision(LevelDivision model,FormCollection fc)
        {
            if (!SessionData.IsAdministrator)
                return ReturnNotAllowed();
          
            EntityProxy ep = new EntityProxy();
            if (ep.EntityChannel.DivisionExists(model.ParentDivisionID, model.Name))
            {
                ModelState.AddModelError("Name", "a Division with this name already exists");
                FillDivisionClassDropdownData(model.DivisionClassID);
                return View("EditSubdivision", model);
            }

            ep.EntityChannel.AddDivision(model.ParentDivisionID, model.Name, model.DivisionClassID);

            return ShowSubdivision(model.ParentDivisionID);
        }

        private void LoadEntityDropdowns()
        {
            SelectList slstGenderList = Passport.Helpers.GeneralWebUtilities.ReturnSelectListFromList(PassportCommon.Lists.Member.MemberLists.GenderList);
            ViewData["GenderList"] = slstGenderList;
        }

        private string BuildDivisionName(int divisionId,int operatorLevel, out int levelsDown,out List<LevelDivision> divisionData)
        {
            string fullname = string.Empty;

            EntityProxy ep = new EntityProxy();
            List<LevelDivision> levelEntities = ep.EntityChannel.BuildDivisionName(divisionId, operatorLevel, out levelsDown, out divisionData,out fullname);

            return fullname;
        }
    }
}
