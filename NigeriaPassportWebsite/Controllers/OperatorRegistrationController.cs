﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Globalization;
using PassportProxy;
using PassportIntraWebsite.Utilities;
using PassportCommon.Models.Operator;
using Passport.Models.Operator;
using log4net;

namespace NigeriaPassportWebsite.Controllers
{
    public class OperatorRegistrationController : BaseController
    {
        private static ILog log = LogManager.GetLogger("NigeriaPassportWebsite.Controllers.OperatorRegistrationController");

        public ActionResult RegisterOperator(string guid = "")
        {
            log.Info("test");
            OperatorRegistration reg = new OperatorRegistration();

            if (!string.IsNullOrWhiteSpace(guid))
            {
                reg.ValidationGUID = guid;

                return SubmitRegistrationGUID(reg);
            }

            return View("OperatorValidation", reg);
        }

        private ActionResult SubmitRegistrationGUID(OperatorRegistration model)
        {
            if (model.ValidationGUID == null)
            {
                ModelState.AddModelError(string.Empty, "Operator registration code cannot be empty, and must have a total length of 32 characters.");
                return View("OperatorValidation", model);
            }

            if (model.ValidationGUID == null ||
                model.ValidationGUID == string.Empty ||
                model.ValidationGUID.Length != 36)
            {
                ModelState.AddModelError("ValidationGUID", "Operator registration code cannot be empty, and must have a total length of 32 characters.");
            }

            VerificationProxy vp = new VerificationProxy();
            bool verified;
            string errormessage;
            if (!vp.VerificationChannel.VerifyGuid(model.ValidationGUID, out verified, out errormessage))
            {
                return RedirectToAction("SystemError", "Error");
            }

            if (!verified)
            {
                ModelState.AddModelError(string.Empty, errormessage);
                return View("OperatorValidation", model);
            }

            return RedirectToAction("OperatorRegistration", "OperatorRegistration", new { guid = model.ValidationGUID });
        }


        [HttpPost]
        public ActionResult RegisterOperator(OperatorRegistration model)
        {
            return SubmitRegistrationGUID(model);
        }

        public ActionResult OperatorRegistration(string guid)
        {
            WebAccountProxy proxy = new WebAccountProxy();
            OperatorRegistration model = new OperatorRegistration();
            
            model.ValidationGUID = guid;
            
            if (!proxy.WebAccountChannel.GetRegistrationEntity(ref model))
            {
                return RedirectToAction("SystemError", "Error");
            }

            LoadOperatorPersonalDetailsDropdown();

            OperatorRegistrationWeb webmodel = (OperatorRegistrationWeb)PassportCommon.Utilities.GeneralUtilities.MapClassReflection(model, typeof(OperatorRegistrationWeb));

            return View("OperatorRegistration", webmodel);
        }

        private void LoadOperatorPersonalDetailsDropdown()
        {
            SelectList slstGenderList = Passport.Helpers.GeneralWebUtilities.ReturnSelectListFromList(PassportCommon.Lists.Member.MemberLists.GenderList);
            ViewData["GenderList"] = slstGenderList;
        }


        [HttpPost]
        public ActionResult OperatorRegistration(OperatorRegistration model)
        {
            WebAccountProxy webaccountproxy = new WebAccountProxy();

            var allErrors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                if (webaccountproxy.WebAccountChannel.UserNameExists(model.UserName, false))
                {
                    ModelState.AddModelError("UserName", "The username is already in use. Please select a unique username.");
                }

                if (model.UserName == model.Password)
                {
                    ModelState.AddModelError("Password", "Username and password cannot be the same.");
                }
            }

            if (!ModelState.IsValid)
            {
                LoadOperatorPersonalDetailsDropdown();

                OperatorRegistrationWeb webModel = (OperatorRegistrationWeb)PassportCommon.Utilities.GeneralUtilities.MapClassReflection(model, typeof(OperatorRegistrationWeb));

                return View(webModel);
            }
            else
            {

                string message = string.Empty;
                Passport.Models.Shared.MessageModel messageModel = new Passport.Models.Shared.MessageModel();

                if (!webaccountproxy.WebAccountChannel.RegisterOperator(model, Passport.Helpers.GeneralWebUtilities.GetUserInfo(), out message))
                {
                    messageModel.MessageHeader = "An error occured with registration, please try again later.";
                    messageModel.Message = message == "" ? "Sorry for the inconvenience." : message;
                    messageModel.ActionLinkController = "Home";
                    messageModel.ActionLinkAction = "Index";
                    messageModel.ActionLinkText = "Go to the home page";
                }
                else
                {
                    messageModel.MessageHeader = "Congratulations! Your account registration was successful.";
                    messageModel.Message = "You can log in using the information provided with registration.";
                    messageModel.ActionLinkController = "Security";
                    messageModel.ActionLinkAction = "Login";
                    messageModel.ActionLinkText = "Go to login";
                }

                return View("ShowMessage", messageModel);
            }
        }

        
        //public ActionResult GetRequestData(int requestID)
        //{
        //    //int bankID = 0;

        //    //if (!int.TryParse(bank, out bankID))
        //    //    throw new ApplicationException("Could not convert BankID to int in ajax action");

        //    EntityProxy ep = new EntityProxy();
        //    var rData = ep.EntityChannel.GetRequest(requestID);

        //    return Json(rData, JsonRequestBehavior.AllowGet);
        //}
    }
}
