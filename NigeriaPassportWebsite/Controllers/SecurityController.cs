﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CaptchaMVC.Models;
using PassportCommon.Models.Security;
using Passport.Models.Session;
using PassportProxy;
using PassportCommon.Enumeration.WebAccount;
using Passport.Helpers;
using Passport.Models.Shared;
using Passport.Models.Security;
using NigeriaPassportWebsite.Models.Security;

namespace NigeriaPassportWebsite.Controllers
{
    public class SecurityController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(CaptchaModel captchaModel, LoginModel loginMod, FormCollection formData)
        {
            if (!PassportCommon.Utilities.GeneralUtilities.IsDeveloper)
            {
                if (!CaptchaMVC.HtmlHelpers.CaptchaHelper.Verify(captchaModel))
                {
                    TempData["error"] = "Please type the captcha code as indicated in the image.";
                    //Invalid

                    ModelState.Remove("CaptchaInputText");
                    return View(loginMod);
                }
            }

            bool viewDashboard = false;

            //if (formData["DashboardView"].StartsWith("true"))
            //    viewDashboard = true;

            return DoInternalLogin(loginMod, loginDashboard: viewDashboard);
        }

        public ActionResult Logout()
        {
            var values = new System.Web.Routing.RouteValueDictionary();
            values.Add("loggedOut", true);
            
            return RedirectToAction("Index", "Home", values);
        }

        public ActionResult DoInternalLogin(LoginModel loginMod, bool loginDashboard = false)
        {
            var allErrors = ModelState.Values.SelectMany(v => v.Errors);

            // This is not a nice solution, but the modelstate is never valid even if set to the tempdata model.
            if (ModelState.IsValid ||
                loginMod == null)
            {
                if (loginMod == null)
                {
                    if (TempData["model"] == null)
                    {
                        return View("Login", new LoginModel());
                    }
                    else
                    {
                        loginMod = (LoginModel)TempData["model"];
                    }
                }

                WebAccountStatus status = WebAccountStatus.New;
                PassportCommon.Models.Security.UserLoginModel model;

                //if (astypeofentity != 0)
                //{
                //    SessionData.CurrentEntityType = astypeofentity;
                //}

                //Check if a proxy isn't used for the IP
                string UserIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null ? Request.ServerVariables["REMOTE_ADDR"].ToString() : Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                SecurityProxy proxy = new SecurityProxy();

                model = new PassportCommon.Models.Security.UserLoginModel();
                model.UserName = loginMod.UserName.ToUpper();
                model.Password = loginMod.Password;
                model.UserIP = UserIP;

                if (!proxy.SecurityChannel.LoginEntityUser(ref model))
                {
                    status = (WebAccountStatus)model.WebAccountStatus;

                    if (status == WebAccountStatus.New)
                    {
                        return RedirectToAction("MemberActivate", "Registration", new { id = model.WebAccountId });
                    }
                    else
                    {
                        ModelState.AddModelError("Password", model.Message);
                        ViewData["LoginError"] = true;
                        return View("Login", loginMod);
                    }
                }
                else
                {
                    AuditTrailProxy auditproxy = new AuditTrailProxy();
                    if (!auditproxy.channel.SetWebAccountActivityOnLogin(model.WebAccountId, UserIP))
                    {
                        ModelState.AddModelError("Password", "A login error has occured.");
                        ViewData["LoginError"] = true;
                        return View("Login", loginMod);
                    }
                    SessionData.UserIP = UserIP;

                    status = (WebAccountStatus)model.WebAccountStatus;

                    SessionData.WebAccountId = model.WebAccountId;
                    SessionData.EntityId = model.EntityId;
                    SessionData.UserName = loginMod.UserName;
                    SessionData.FirstName = model.FirstName;
                    SessionData.DivisionID = model.DivisionID;
                    SessionData.DivisionName = model.DivisionName;
                    SessionData.DivisionLevel = model.DivisionLevel;
                    SessionData.IsAdministrator = model.IsAdministrator;
                    SessionData.PassportVerificationAllowed = model.PassportVerificationAllowed;

                    PassportCommon.Models.WebAccount.WebAccountLoginStatus webaccountloginstatus = new PassportCommon.Models.WebAccount.WebAccountLoginStatus(SessionData.WebAccountId);
                    if (!proxy.SecurityChannel.GetLoginDetailStatus(ref webaccountloginstatus))
                    {
                        return RedirectToAction("SystemError", "Error");
                    }

                    SessionData.CurrentLoginDetailStatus = webaccountloginstatus.LOGIN_DETAIL_STATUS;
                    
                    if ((WebAccountStatus)model.WebAccountStatus == WebAccountStatus.Active)
                    {
                        if(model.IsAdministrator)
                            return RedirectToAction("EntityHome", "Entity");
                        else if(model.PassportVerificationAllowed)
                            return RedirectToAction("VerifyPassport", "PassportVerification");
                        
                        return RedirectToAction("Index", "Home");
                    }
                    else if ((WebAccountStatus)model.WebAccountStatus == WebAccountStatus.SecurityQuestions)
                    {
                        return RedirectToAction("SecurityQuestions", "Security", new { id = model.WebAccountId });
                    }                    
                }
                return View("Login", loginMod);
            }
            else
            {
                return View("Login", loginMod);
            }
        }

        public ActionResult ShowLoggedOutStatus()
        {
            SessionData.Logout();
            Session.Clear();
            Session.Abandon();

            MessageModel model = new MessageModel();
            model.MessageHeader = "Authentication required";
            model.Message = "You have been logged out of the system, if you wish to log in again please click the link below to go back to the login screen.";
            model.ActionLinkAction = "Login";
            model.ActionLinkController = "Security";
            model.ActionLinkText = "Login";

            return View("ShowMessage", model);
        }

        #region Security Questions
        [RestrictAccess]
        public ActionResult SecurityQuestions(long id)
        {
            List<PassportCommon.Models.WebAccount.SecurityQuestion> questions = SetSecurityQuestionData(id);

            return View(questions);
        }

        [RestrictAccess]
        private List<PassportCommon.Models.WebAccount.SecurityQuestion> SetSecurityQuestionData(long id)
        {
            WebAccountProxy pxy = new WebAccountProxy();
            List<PassportCommon.Models.WebAccount.SecurityQuestion> questions = pxy.WebAccountChannel.GetSecurityQuestions();

            List<SelectListItem> itemList = (from c in questions
                                             select new SelectListItem() { Text = c.QuestionText, Value = c.ID.ToString() }
                                            ).ToList();

            ViewData["SecurityQuestions"] = itemList;
            ViewData["Question2"] = new List<SelectListItem>();
            ViewData["webAccountID"] = id;
            return questions;
        }

        [RestrictAccess]
        [HttpPost]
        public ActionResult SecurityQuestions(long id, int? Question1, string Question1Answer, int? Question2, string Question2Answer)
        {
            if (Question1 == null || Question1 == 0)
            {
                ModelState.AddModelError("Question1", "Please select a question.");
                SetSecurityQuestionData(id);
                return View();
            }

            if (Question2 == null || Question2 == 0)
            {
                ModelState.AddModelError("Question2", "Please select a question.");
                SetSecurityQuestionData(id);
                return View();
            }

            if (string.IsNullOrEmpty(Question1Answer))
            {
                ModelState.AddModelError("Question1Answer", "Please provide an answer to the question.");
                SetSecurityQuestionData(id);
                return View();
            }

            if (string.IsNullOrEmpty(Question2Answer))
            {
                ModelState.AddModelError("Question2Answer", "Please provide an answer to the question.");
                SetSecurityQuestionData(id);
                return View();
            }

            if (Question1Answer.Length > 25)
            {
                ModelState.AddModelError("Question1Answer", "Please provide an answer that is less than 25 characters. ");
                SetSecurityQuestionData(id);
                return View();
            }

            if (Question2Answer.Length > 25)
            {
                ModelState.AddModelError("Question2Answer", "Please provide an answer that is less than 25 characters. ");
                SetSecurityQuestionData(id);
                return View();
            }

            List<PassportCommon.Models.WebAccount.SecurityQuestion> answerList = new List<PassportCommon.Models.WebAccount.SecurityQuestion>();
            answerList.Add(new PassportCommon.Models.WebAccount.SecurityQuestion() { ID = (int)Question1, UserAnswer = Question1Answer });
            answerList.Add(new PassportCommon.Models.WebAccount.SecurityQuestion() { ID = (int)Question2, UserAnswer = Question2Answer });

            WebAccountProxy pxy = new WebAccountProxy();
            pxy.WebAccountChannel.SaveSecurityQuestions(id, answerList);

            EntityProxy ep = new EntityProxy();
            //ep.EntityChannel.SetWebaccountStatus(id,WebAccountStatus.Active);

            ep.EntityChannel.SetOperatorRoles(id);

            SessionData.RefreshRoles();
     
            return RedirectToAction("Index", "Home");     
        }        

        [RestrictAccess]
        public ActionResult GetSecurityQuestions(string selectedQuestion)
        {
            WebAccountProxy pxy = new WebAccountProxy();
            List<PassportCommon.Models.WebAccount.SecurityQuestion> questions = pxy.WebAccountChannel.GetSecurityQuestions();

            List<SelectListItem> itemList = (from c in questions
                                             where c.ID.ToString() != selectedQuestion
                                             select new SelectListItem() { Text = c.QuestionText, Value = c.ID.ToString() }
                                            ).ToList();

            ViewData["Question2"] = new List<SelectListItem>();

            return Json(itemList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Web Account Maintentance

        [RestrictAccess]
        public ActionResult WebAccountMaintenance()
        {
            SecurityProxy proxy = new SecurityProxy();
            PassportCommon.Models.Security.WebAccountMaintenance webAccountMaintenance;

            WebAccountMaintenanceViewModel webAccountMaintenanceViewModel = new WebAccountMaintenanceViewModel();

            if (!proxy.SecurityChannel.GetWebAccountMaintenanceData(SessionData.WebAccountId, out webAccountMaintenance))
            {
                return RedirectToAction("SystemError", "Error");
            }

            webAccountMaintenanceViewModel = (WebAccountMaintenanceViewModel)PassportCommon.Utilities.GeneralUtilities.MapClassReflection(webAccountMaintenance, typeof(WebAccountMaintenanceViewModel));

            webAccountMaintenanceViewModel.LOGIN_NAME = WebAccountMaintenanceViewModel.UserMask;
            webAccountMaintenanceViewModel.Password = WebAccountMaintenanceViewModel.PasswordMask;
            webAccountMaintenanceViewModel.RetypePassword = WebAccountMaintenanceViewModel.PasswordMask;

            return View(webAccountMaintenanceViewModel);
        }

        [RestrictAccess]
        [HttpPost]
        public ActionResult WebAccountMaintenance(WebAccountMaintenanceViewModel webAccountMaintenanceViewModel)
        {
            if (ModelState.IsValid)
            {
                if (webAccountMaintenanceViewModel.Password == WebAccountMaintenanceViewModel.PasswordMask)
                {
                    ModelState.AddModelError("Password", "Please provide a secure password.");
                }

                if (webAccountMaintenanceViewModel.Password == webAccountMaintenanceViewModel.LOGIN_NAME)
                {
                    ModelState.AddModelError("Password", "Username and password cannot be the same.");
                }
            }

            if (!ModelState.IsValid)
            {
                webAccountMaintenanceViewModel.LOGIN_NAME = WebAccountMaintenanceViewModel.UserMask;
                webAccountMaintenanceViewModel.Password = WebAccountMaintenanceViewModel.PasswordMask;
                webAccountMaintenanceViewModel.RetypePassword = WebAccountMaintenanceViewModel.PasswordMask;
                return View(webAccountMaintenanceViewModel);
            }

            PassportCommon.Models.Security.WebAccountMaintenance webAccountMaintenance = (PassportCommon.Models.Security.WebAccountMaintenance)PassportCommon.Utilities.GeneralUtilities.MapClassReflection(webAccountMaintenanceViewModel, typeof(PassportCommon.Models.Security.WebAccountMaintenance));

            SecurityProxy proxy = new SecurityProxy();
            if (!proxy.SecurityChannel.UpdateWebAccountMaintenanceData(webAccountMaintenance))
            {
                return RedirectToAction("SystemError", "Error");
            }

            ViewData["Message"] = "Your web account has been updated successfully.";
            return View("Message");
        }


        [RestrictAccess]
        public ActionResult AuthenticateChangeUserDetails(PassportCommon.Enumeration.Security.UserDetailsChangeType userDetailsChangeType)
        {
            PassportCommon.Models.Security.UserLoginDetailsChange model = new PassportCommon.Models.Security.UserLoginDetailsChange();
            model.ChangeType = userDetailsChangeType;

            return View(model);
        }


        [RestrictAccess]
        [HttpPost]
        public ActionResult AuthenticateChangeUserDetails(PassportCommon.Models.Security.UserLoginDetailsChange model)
        {
            SecurityProxy proxy = new SecurityProxy();

            if (ModelState.IsValid)
            {
                if (!proxy.SecurityChannel.IsLoginValidForEntityId(model.UserName.ToUpper(), model.Password, SessionData.EntityId))
                {
                    ModelState.AddModelError("Password", "An error occured with the login details validation, please make sure your user name and password are correct.");
                }
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                string errormessage;
                ChangeUserDetails changeuserdetails;
                switch (model.ChangeType)
                {
                    case (PassportCommon.Enumeration.Security.UserDetailsChangeType.UserName):
                        ChangeUsername changeusername = new ChangeUsername();
                        changeusername.WebAccountId = SessionData.WebAccountId;
                        proxy.SecurityChannel.SendUserLostMessage(SessionData.WebAccountId, out errormessage);
                        return View("ChangeUsername", changeusername);
                    case (PassportCommon.Enumeration.Security.UserDetailsChangeType.Password):
                        ChangePassword changepassword = new ChangePassword();
                        changepassword.WebAccountId = SessionData.WebAccountId;
                        proxy.SecurityChannel.SendPasswordLostMessage(SessionData.WebAccountId, out errormessage);
                        return View("ChangePassword", changepassword);
                    case (PassportCommon.Enumeration.Security.UserDetailsChangeType.Email):
                        changeuserdetails = new ChangeUserDetails();
                        return View("ChangeUserDetails", changeuserdetails);
                }
            }
            return RedirectToAction("SystemError", "Error");
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePassword passwordData)
        {
            if (!ModelState.IsValid)
            {
                return View(passwordData);
            }

            if (passwordData.NewPassword != passwordData.NewPasswordRetype)
            {
                ModelState.AddModelError("NewPassword", "The password and the retyped passwords does not match.");
            }

            SecurityProxy proxy = new SecurityProxy();
            string error;
            if (!proxy.SecurityChannel.ChangePassword(passwordData.WebAccountId, passwordData.NewPassword, passwordData.SecurityCode, out error))
            {
                return RedirectToAction("SystemError", "Error");
            }

            if (error != string.Empty)
            {
                ModelState.AddModelError("NewPassword", error);

                return View(passwordData);
            }

            MessageModel messageModel = new MessageModel();
            if (!SessionData.IsAuthenticated)
            {
                SecurityProxy securityproxy = new SecurityProxy();
                securityproxy.SecurityChannel.WebAccountRecoveryLog(passwordData.WebAccountId, WebAccountRecoveryLogType.User_password_recovery, PassportIntraWebsite.Utilities.GeneralWebUtilities.GetUserInfo());

                messageModel.MessageHeader = "Congratulations! Your password update was successful and you can now log in";
                messageModel.Message = "You can now use your login name and new password to log in.";
                messageModel.ActionLinkController = "Security";
                messageModel.ActionLinkAction = "Login";
                messageModel.ActionLinkText = "Go to login";
            }
            else
            {
                SecurityProxy securityproxy = new SecurityProxy();
                securityproxy.SecurityChannel.WebAccountRecoveryLog(passwordData.WebAccountId, WebAccountRecoveryLogType.User_password_change, PassportIntraWebsite.Utilities.GeneralWebUtilities.GetUserInfo());

                messageModel.MessageHeader = "Password update was successful";
                messageModel.Message = "You can now use your login name and new password next time you log in.";
                messageModel.ActionLinkController = "Home";
                messageModel.ActionLinkAction = "Index";
                messageModel.ActionLinkText = "Go Home";
            }

            return View("ShowMessage", messageModel);
        }


        [HttpPost]
        public ActionResult ChangeUsername(ChangeUsername usernameData)
        {
            if (!ModelState.IsValid)
            {
                return View(usernameData);
            }

            if (usernameData.NewUserName != usernameData.NewUserNameRetype)
            {
                ModelState.AddModelError("NewUserName", "The username and the retyped username does not match.");
            }

            WebAccountProxy pxy = new WebAccountProxy();

            if (pxy.WebAccountChannel.UserNameExists(usernameData.NewUserName, false))
            {
                ModelState.AddModelError("NewUserName", "The username is already in use. Please select a unique username or use your gsm number/email address.");
                return View(usernameData);
            }

            SecurityProxy proxy = new SecurityProxy();
            string error;
            if (!proxy.SecurityChannel.ChangeUser(usernameData.WebAccountId, usernameData.NewUserName, usernameData.SecurityCode, out error))
            {
                return RedirectToAction("SystemError", "Error");
            }

            if (error != string.Empty)
            {
                ModelState.AddModelError("NewUserName", error);

                return View(usernameData);
            }

            MessageModel messageModel = new MessageModel();
            if (!SessionData.IsAuthenticated)
            {
                SecurityProxy securityproxy = new SecurityProxy();
                securityproxy.SecurityChannel.WebAccountRecoveryLog(usernameData.WebAccountId, WebAccountRecoveryLogType.User_username_recovery, PassportIntraWebsite.Utilities.GeneralWebUtilities.GetUserInfo());

                messageModel.MessageHeader = "Congratulations! Your username update was successful and you can now log in";
                messageModel.Message = "You can now use your new username and password to log in.";
                messageModel.ActionLinkController = "Security";
                messageModel.ActionLinkAction = "Login";
                messageModel.ActionLinkText = "Go to login";
            }
            else
            {
                SecurityProxy securityproxy = new SecurityProxy();
                securityproxy.SecurityChannel.WebAccountRecoveryLog(usernameData.WebAccountId, WebAccountRecoveryLogType.User_username_change, PassportIntraWebsite.Utilities.GeneralWebUtilities.GetUserInfo());

                messageModel.MessageHeader = "Username update was successful";
                messageModel.Message = "You can now use your new login name and password next time you log in.";
                messageModel.ActionLinkController = "Home";
                messageModel.ActionLinkAction = "Index";
                messageModel.ActionLinkText = "Go Home";
            }

            return View("ShowMessage", messageModel);
        }


        [RestrictAccess]
        [HttpPost]
        public ActionResult ChangeUserDetails(ChangeUserDetails model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.NewEmail == string.Empty)
            {
                ModelState.AddModelError("NewEmail", "Email address is required.");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SecurityProxy proxy = new SecurityProxy();

            if (!proxy.SecurityChannel.UpdateEntityEmail(SessionData.EntityId, model.NewEmail))
            {
                return RedirectToAction("SystemError", "Error");
            }

            Passport.Models.Shared.MessageModel messageModel = new Passport.Models.Shared.MessageModel();

            messageModel.MessageHeader = "Email address update";
            messageModel.Message = "Your email address has been changed successfully.";
            messageModel.ActionLinkController = "Home";
            messageModel.ActionLinkAction = "Index";
            messageModel.ActionLinkText = "Go Home";

            return View("ShowMessage", messageModel);
        }

        #endregion

        #region Lost username or password
        public ActionResult LostPassword()
        {
            PassportCommon.Models.Security.LostPassword model = new PassportCommon.Models.Security.LostPassword();
            return View(model);
        }

        [HttpPost]
        public ActionResult LostPassword(PassportCommon.Models.Security.LostPassword model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SecurityProxy proxy = new SecurityProxy();
            if (!proxy.SecurityChannel.VerifyLostPasswordUserDetails(ref model, true))
            {
                return RedirectToAction("SystemError", "Error");
            }
            else
            {
                if (!string.IsNullOrEmpty(model.ErrorMessage))
                {
                    ModelState.AddModelError("", model.ErrorMessage);
                    return View(model);
                }
                
                ChangePassword changepassword = new ChangePassword();
                changepassword.WebAccountId = model.WebAccountId;
                return View("ChangePassword", changepassword);
            }
        }

        public ActionResult LostUsername()
        {
            LostUserName model = new LostUserName();
            return View(model);
        }

        [HttpPost]
        public ActionResult LostUsername(LostUserName model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SecurityProxy proxy = new SecurityProxy();
            if (!proxy.SecurityChannel.VerifyLostUsernameUserDetails(ref model, true))
            {
                return RedirectToAction("SystemError", "Error");
            }
            else
            {
                if (!string.IsNullOrEmpty(model.ErrorMessage))
                {
                    ModelState.AddModelError("", model.ErrorMessage);
                    return View(model);
                }

                ChangeUsername changeusername = new ChangeUsername();
                changeusername.WebAccountId = model.WebAccountId;
                return View("ChangeUsername", changeusername);
            }
        }

        public ActionResult ResendUserCode()
        {
            SecurityProxy proxy = new SecurityProxy();
            string errormessage;
            proxy.SecurityChannel.SendUserLostMessage(SessionData.WebAccountId, out errormessage);

            if (!string.IsNullOrEmpty(errormessage))
            {
                ModelState.AddModelError("", errormessage);
            }

            ChangeUsername changeusername = new ChangeUsername();
            changeusername.WebAccountId = SessionData.WebAccountId;
            return View("ChangeUsername", changeusername);
        }


        public ActionResult ResendPasswordCode()
        {
            SecurityProxy proxy = new SecurityProxy();
            string errormessage;
            proxy.SecurityChannel.SendPasswordLostMessage(SessionData.WebAccountId, out errormessage);

            if (!string.IsNullOrEmpty(errormessage))
            {
                ModelState.AddModelError("", errormessage);
            }

            ChangePassword changepassword = new ChangePassword();
            changepassword.WebAccountId = SessionData.WebAccountId;
            return View("ChangePassword", changepassword);
        }
        #endregion

        #region Lost login details
        public ActionResult ResetLostUserLoginDetailsUser()
        {
            LostLoginUserDetails model = new LostLoginUserDetails();
            return View(model);
        }

        public ActionResult LostUserLoginDetailSearchResultUser(LostLoginUserDetails userdetail)
        {
            LostLoginDetailsReset lostlogindetailreset;    
            LoginDetailResetLimited logindetailresetlimited = new LoginDetailResetLimited();
            SecurityProxy proxy = new SecurityProxy();

            if (!proxy.SecurityChannel.VerifyLostLoginDetailUser(userdetail, out lostlogindetailreset))
            {
                return RedirectToAction("SystemError", "Error");
            }

            if (lostlogindetailreset.Verified && lostlogindetailreset.WebAccountId > 0)
            {
                if (!proxy.SecurityChannel.GetLoginDetailResetInfo(lostlogindetailreset.WebAccountId, out logindetailresetlimited))
                {
                    return RedirectToAction("SystemError", "Error");
                }
            }
            else
            {
                MessageModel message = new MessageModel();
                message.MessageHeader = "User not found.";
                message.Message = "The system did not find a registered user with this email address.";
                message.ActionLinkAction = "Index";
                message.ActionLinkController = "Home";
                message.ActionLinkText = "Home";

                return View("ShowMessagePartial", message);
            }

            SessionData.SessionCache["VerifiedLostLoginDetailResetData"] = lostlogindetailreset;
            LostLoginDetailsResetUser model = GetLoginDetailResetUserModel(lostlogindetailreset);
            model.ReachedIncorrectAnswerLimit = logindetailresetlimited.ReachedIncorrectAnswerLimit;

            return View(model);
        }

        [HttpPost]
        public JsonResult VerifySecurityQuestionAnswer(long entityId, int displaysecurityquestionnumber, string inputanswer)
        {
            bool verified = false;
            Dictionary<string, object> result = new Dictionary<string, object>();
            string failedverificationmessage = "<p><span style=\"float:left; margin:0 0px 20px 0;\"></span>The security question verification failed, please try again.</p>";
            string verificationerrormessage = "<p><span style=\"float:left; margin:0 0px 20px 0;\"></span>A system error occured with verification, please try again later.</p>";

            if (SessionData.SessionCache["VerifiedLostLoginDetailResetData"] == null)
            {
                result.Add("verificationerror", true);
                result.Add("message", verificationerrormessage);
            }
            else
            {
                LostLoginDetailsReset lostlogindetailreset = (LostLoginDetailsReset)SessionData.SessionCache["VerifiedLostLoginDetailResetData"];
                if (lostlogindetailreset.EntityId != entityId)
                {
                    result.Add("verificationerror", true);
                    result.Add("message", verificationerrormessage);
                }
                else
                {
                    if (displaysecurityquestionnumber % 2 != 0)
                    {
                        if (CompareSecurityQuestionAnswer(lostlogindetailreset.SecurityQuestionAnswer1, inputanswer))
                        {
                            verified = true;
                        }
                    }
                    else //Even number
                    {
                        if (!CompareSecurityQuestionAnswer(lostlogindetailreset.SecurityQuestionAnswer2, inputanswer))
                        {
                            verified = true;
                        }
                    }

                    SecurityProxy proxy = new SecurityProxy();
                    if (verified)
                    {
                        if (!proxy.SecurityChannel.ResetLostLoginDetail(ref lostlogindetailreset, GeneralWebUtilities.GetUserInfo()))
                        {
                            result.Add("reseterror", true);
                            result.Add("message", verificationerrormessage);
                        }
                        else
                        {
                            SessionData.SessionCache.Remove("VerifiedLostLoginDetailResetData");
                            result.Add("resetsuccess", true);
                            result.Add("message", "<p><span style=\"float:left; margin:0 0px 20px 0;\"></span>Your login details have been reset successfully and the details have been emailed to you. Please follow the instructions in the email to log into the website.</p>");
                        }
                    }
                    else
                    {
                        LoginDetailResetLimited logindetailresetlimited;
                        if (!proxy.SecurityChannel.IncrementLoginDetailResetIncorrectTries(lostlogindetailreset.WebAccountId, out logindetailresetlimited))
                        {
                            result.Add("reseterror", true);
                            result.Add("message", verificationerrormessage);
                        }
                        else
                        {
                            if (logindetailresetlimited.ReachedIncorrectAnswerLimit)
                            {
                                result.Add("resetlimitreached", true);
                                result.Add("message", "<p><span style=\"float:left; margin:0 0px 20px 0;\"></span>The limit for incorrect security question answers has been reached and this account has been suspended. Please contact the administrator.</p>");
                            }
                            else
                            {
                                result.Add("verificationerror", true);
                                result.Add("message", failedverificationmessage);
                            }
                        }
                    }
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private bool CompareSecurityQuestionAnswer(string savedanswer, string inputanswer)
        {
            //Remove all whitespace before comparison
            savedanswer = String.Join("", savedanswer.Where(c => !char.IsWhiteSpace(c)));
            inputanswer = String.Join("", inputanswer.Where(c => !char.IsWhiteSpace(c)));

            return inputanswer.Equals(savedanswer, StringComparison.OrdinalIgnoreCase);
        }

        private LostLoginDetailsResetUser GetLoginDetailResetUserModel(LostLoginDetailsReset lostlogindetailreset)
        {
            LostLoginDetailsResetUser model = new LostLoginDetailsResetUser();

            model.Verified = lostlogindetailreset.Verified;
            model.SecurityQuestionsExist = lostlogindetailreset.SecurityQuestionsExist;
            model.EntityId = lostlogindetailreset.EntityId;
            model.WebAccountStatus = lostlogindetailreset.WebAccountStatus;
            model.Email = lostlogindetailreset.Email;
            model.SecurityQuestion1 = lostlogindetailreset.SecurityQuestion1;
            model.SecurityQuestion2 = lostlogindetailreset.SecurityQuestion2;

            return model;
        }

        public ActionResult ChangeLoginDetails()
        {
            Passport.Models.Security.LoginDetailsChangeWeb model = new LoginDetailsChangeWeb();
            model.LoginDetailStatus = SessionData.CurrentLoginDetailStatus;

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeLoginDetails(Passport.Models.Security.LoginDetailsChangeWeb model)
        {
            SecurityProxy securityproxy = new SecurityProxy();
            WebAccountProxy webaccountproxy = new WebAccountProxy();

            var allErrors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                switch (model.LoginDetailStatus)
                {
                    case LoginDetailStatus.Change_username_on_next_login:
                        if (string.IsNullOrWhiteSpace(model.NewUserName))
                        {
                            ModelState.AddModelError("NewUserName", "Username is required.");
                        }

                        break;
                    case LoginDetailStatus.Change_password_on_next_login:
                        if (string.IsNullOrWhiteSpace(model.NewPassword))
                        {
                            ModelState.AddModelError("NewPassword", "Password is required.");
                        }
                        break;
                    case LoginDetailStatus.Change_username_and_password_on_next_login:
                        if (string.IsNullOrWhiteSpace(model.NewUserName))
                        {
                            ModelState.AddModelError("NewUserName", "Username is required.");
                        }

                        if (string.IsNullOrWhiteSpace(model.NewPassword))
                        {
                            ModelState.AddModelError("NewPassword", "Password is required.");
                        }
                        break;
                }
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            LoginDetailsChange newmodel = (LoginDetailsChange)PassportCommon.Utilities.GeneralUtilities.MapClassReflection(model, typeof(LoginDetailsChange));

            newmodel.WebAccountId = SessionData.WebAccountId;

            if (securityproxy.SecurityChannel.ChangeLoginDetails(ref newmodel))
            {
                model.ShowMessage = newmodel.ShowMessage;
                model.ChangeSuccessful = newmodel.ChangeSuccessful;
                model.Message = newmodel.Message;

                if (!newmodel.ChangeSuccessful)
                {
                    model.ShowMessage = true;
                    if (!string.IsNullOrEmpty(newmodel.Message))
                    {
                        model.Message = newmodel.Message;
                    }
                    else
                    {
                        model.Message = "An error occured when changing your user details, please try again.";
                    }
                    return View(model);
                }
                else
                {
                    SessionData.CurrentLoginDetailStatus = LoginDetailStatus.No_username_or_password_change_required;
                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("SystemError", "Error");
            }
        }
        #endregion
    }
}
