﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Exaro.Data;
using PassportProxy;

namespace NigeriaPassportWebsite.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
                return;
            
            var ex = filterContext.Exception ?? new Exception("No information Available.");
            try
            {
                SecurityProxy sp = new SecurityProxy();
                sp.SecurityChannel.LogError(MapException(ex));
            }
            catch(Exception exception)  
            {
                     
            }
        }

        internal ExaroSystemException MapException(Exception ex)
        {
            ExaroSystemException exception = new ExaroSystemException();
            exception.MachineNameOrIP = Request.UserHostAddress;
            exception.Message = string.IsNullOrEmpty(ex.Message) ? "" : ex.Message;
            exception.Stacktrace = ex.StackTrace == null ? "" : ex.StackTrace.ToString();

            if (ex.InnerException != null)
            {
                exception.InnerExceptionMessage = string.IsNullOrEmpty(ex.InnerException.Message) ? "" : ex.InnerException.Message;
                exception.InnerExceptionStackTrace = ex.InnerException.StackTrace == null ? "" : ex.InnerException.StackTrace.ToString();
            }

            try
            {
                exception.Browser = Request.Browser.Browser + " " + Request.Browser.MajorVersion + "." + Request.Browser.MinorVersion;

                try
                {
                    exception.Browser += " Js " + Request.Browser["JavaScriptVersion"];
                }
                catch { } 
            }
            catch { }

            return exception;
        }
    }
}