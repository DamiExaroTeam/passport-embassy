﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Collections;

namespace Passport.Cache
{
    public class SessionCache
    {
        #region Fields and Properties

        private string _uniqueId;

        public object this[string key]
        {
            get 
            { 
                return HttpRuntime.Cache[CreateKey(key)]; 
            }
            set 
            {
                Insert(key, value);
            }
        }

        #endregion

        #region Constructors

        public SessionCache()
        {         
            if (HttpContext.Current == null)
            {
                Init(Guid.NewGuid().ToString());
            }
            else
            {
                Init(HttpContext.Current.Session.SessionID);
            }
        }

        public SessionCache(string uniqueId)
        {
            Init(uniqueId);
        }

        private void Init(string uniqueId)
        {
            if (string.IsNullOrEmpty(uniqueId))
            {
                throw new ArgumentNullException("uniqueId");
            }
            _uniqueId = uniqueId;
        }

        #endregion

        #region Methods

        public void Insert(string key, object data)
        {
            HttpRuntime.Cache.Add(CreateKey(key), data, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 15, 0), CacheItemPriority.High, null);
        }

        public void Insert(string key, object data, TimeSpan expiry)
        {
            HttpRuntime.Cache.Add(CreateKey(key), data, null, System.Web.Caching.Cache.NoAbsoluteExpiration, expiry, CacheItemPriority.High, null);
        }

        public void Remove(string key)
        {
            HttpRuntime.Cache.Remove(CreateKey(key));
        }

        #endregion

        #region Private Methods

        private string CreateKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }
            return string.Format("{0}:{1}", key, _uniqueId);
        }

        #endregion

        public void ClearSessionCache()
        {
            List<string> keys = new List<string>(); 

            IDictionaryEnumerator enumerator = HttpRuntime.Cache.GetEnumerator(); 
            while (enumerator.MoveNext()) 
            {
                string key = enumerator.Key.ToString();
                if (key.EndsWith(_uniqueId))
                {
                    keys.Add(enumerator.Key.ToString());
                }
            } 
 
            for (int i = 0; i < keys.Count; i++) 
            { 
                HttpRuntime.Cache.Remove(keys[i]); 
            } 
        }
    }
}