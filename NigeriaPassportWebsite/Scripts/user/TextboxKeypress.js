﻿function KeypressNumbersOnly(event) {
    var keycode = ('which' in event) ? event.which : event.keyCode;
    var letter = String.fromCharCode(keycode);
    var patt = new RegExp("[0-9]");

    if (keycode != 8 && //backspace 
        keycode != 9 && //tabspace
        keycode != 46 && //delete
        (keycode < 37 || keycode > 40)) //arrows
    {
        if (!patt.test(letter)) {
            return false;
        }
    }
}

function KeypressAlpaNumericOnly(event) {
    var keycode = ('which' in event) ? event.which : event.keyCode;
    var letter = String.fromCharCode(keycode);
    var patt = new RegExp("[A-z0-9]");

    if (keycode != 8 && //backspace 
        keycode != 9 && //tabspace
        keycode != 46 && //delete
        (keycode < 37 || keycode > 40)) //arrows
    {
        if (!patt.test(letter)) {
            return false;
        }
    }
}

function KeypressEmail(e) {
    var keycode = ('which' in e) ? e.which : e.keyCode;
    var letter = String.fromCharCode(keycode);
    var patt = new RegExp("^[.@A-z0-9_-]*$");

    if (keycode != 8 && //backspace 
        keycode != 9 && //tabspace
        (keycode < 37 || keycode > 40) //arrows
       ) {
        if (!patt.test(letter)) {
            return false;
        }
    }
}


function IsValidEmail(id) {
    var email = document.getElementById(id);
    var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!pattern.test(email.value)) {
        alert('Please provide a valid email address');
        email.focus;
        return false;
    }
}
